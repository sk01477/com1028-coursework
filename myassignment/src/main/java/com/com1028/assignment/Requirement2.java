/**
 * Requirement2.java 
 */
package com.com1028.assignment;

/**
 * @author Shirshendu Karmakar
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.com1028.assignment.Employee;

public class Requirement2 {

	//Fields set to default values 
	protected Connection con = null;
	private Statement statement = null;
	private ResultSet results = null;
	private List<Employee> EmployeeList = null;
	private List<Employee> EmployeeQuery = null;
	//db set to connect to database
	private String db = "jdbc:mysql://localhost:3306/classicmodels?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private String username = null;
	private String password = null;

	/**
	 * Constructor for this class
	 * 
	 * @param username for connecting to database
	 * 
	 * @param password for connecting to database
	 */
	public Requirement2 (String username, String password) {
		super();
		this.username = username;
		this.password = password;
		this.EmployeeList = new ArrayList<Employee>();
		this.EmployeeQuery = new ArrayList<Employee>();

		//Error handling for database connection
		if (username != null && password != null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ());
				con = DriverManager.getConnection(db, username, password);
				System.out.println("Database connection successful");
			}
			catch(Exception e) {
				System.out.println("Database connection failed. Error " + e);
			}
		}

	}

	//Checks the connection
	public boolean checkCon() {
		try {
			if (con.isClosed() == true) {
				return false;
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
		return true;

	}

	//Retrieves and stores data from employees table
	public void getEmployees() {
		try {
			String query = "select * from employees";
			Statement statement = con.createStatement();
			ResultSet results = statement.executeQuery(query);

			while (results.next()) {

				/**
				 * This code will assign the results from the SQL query to their own variable
				 */
				int employeeNumber = results.getInt("employeeNumber");
				String firstName = results.getString("firstName");
				String lastName = results.getString("lastName");
				String extension = results.getString("extension");
				String email = results.getString("email");
				int officeCode = results.getInt("officeCode");
				int reportsTo = results.getInt("reportsTo");
				String jobTitle = results.getString("jobTitle");

				Employee tempEmployee = new Employee(employeeNumber, lastName, firstName, extension,
						email, officeCode, reportsTo, jobTitle);
				EmployeeList.add(tempEmployee);
			}

		}
		catch (SQLException e) {
			System.out.println("An error occurred while making this query.");
			throw new RuntimeException(e);
		} 

	}

	//Executes filtered SQL query to collect list of employees in Boston from employees table
	public void getEmployeeQuery() {
		try {
			String query = "select * from employees where officecode = 2";
			Statement statement = con.createStatement();
			ResultSet results = statement.executeQuery(query);

			while (results.next()) {

				/**
				 * This code will assign the results from the SQL query to their own variable
				 */
				int employeeNumber = results.getInt("employeeNumber");
				String lastName = results.getString("lastName");
				String firstName = results.getString("firstName");
				String extension = results.getString("extension");
				String email = results.getString("email");
				int officeCode = results.getInt("officeCode");
				int reportsTo = results.getInt("reportsTo");
				String jobTitle = results.getString("jobTitle");

				Employee tempQuery = new Employee(employeeNumber, lastName, firstName, extension,
						email, officeCode, reportsTo, jobTitle);
				EmployeeQuery.add(tempQuery);
			}

		}
		catch (SQLException e) {
			System.out.println("An error occurred while making this query.");
			throw new RuntimeException(e);
		} 

	}

	//Outputs all employees working in Boston through a String Buffer
	public String inBoston() {
		StringBuffer Buffer = new StringBuffer();

		for (Employee eachEmployee : EmployeeList) {
			if (eachEmployee.getOfficeCode() == 2) {
				Buffer.append("Employee Number: " + eachEmployee.getEmployeeNumber() + "|" + 
						"Last Name: " + eachEmployee.getLastName() + "|" + "First Name: " + eachEmployee.getFirstName() + "\n" +
						"Extension: " + eachEmployee.getExtension() + "|" + "Email: " + eachEmployee.getEmail() + "\n" +
						"Office Code: " + eachEmployee.getOfficeCode() + "|" + "Reports To: " + eachEmployee.getReportsTo() + "|" +
						"Job Title: " + eachEmployee.getJobTitle() + "\n");
			}
		}

		return "Employees in Boston:" + "\n" + Buffer.toString();

	}

	//Outputs the data from the filtered query through a String Buffer
	public String inBostonQuery() {
		StringBuffer Buffer = new StringBuffer();
		for (Employee eachEmployee : EmployeeQuery) {
			Buffer.append("Employee Number: " + eachEmployee.getEmployeeNumber() + "|" + 
					"Last Name: " + eachEmployee.getLastName() + "|" + "First Name: " + eachEmployee.getFirstName() + "\n" +
					"Extension: " + eachEmployee.getExtension() + "|" + "Email: " + eachEmployee.getEmail() + "\n" +
					"Office Code: " + eachEmployee.getOfficeCode() + "|" + "Reports To: " + eachEmployee.getReportsTo() + "|" +
					"Job Title: " + eachEmployee.getJobTitle() + "\n");
		}

		return "Employees in Boston:" + "\n" + Buffer.toString();

	}

}
