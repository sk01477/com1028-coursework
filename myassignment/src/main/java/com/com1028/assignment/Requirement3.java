/**
 * Requirement3.java 
 */
package com.com1028.assignment;

/**
 * @author Shirshendu Karmakar
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.com1028.assignment.Customer;
import com.com1028.assignment.Orders;
import com.com1028.assignment.Payment;

public class Requirement3 {

	//Fields set to default values 
	protected Connection con = null;
	private Statement statement = null;
	private ResultSet results = null;
	private List<Customer> CustomerList = null;
	private List<Customer> CustomerQuery = null;
	private List<Orders> OrdersList = null;
	private List<Orders> OrdersQuery = null;
	private List<Payment> PaymentList = null;
	private List<Payment> PaymentQuery = null;
	//db set to connect to database
	private String db = "jdbc:mysql://localhost:3306/classicmodels?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private String username = null;
	private String password = null;

	/**
	 * Constructor for this class
	 * 
	 * @param username for connecting to database
	 * 
	 * @param password for connecting to database
	 */
	public Requirement3 (String username, String password) {
		super();
		this.username = username;
		this.password = password;
		this.CustomerList = new ArrayList<Customer>();
		this.CustomerQuery = new ArrayList<Customer>();
		this.OrdersList = new ArrayList<Orders>();
		this.OrdersQuery = new ArrayList<Orders>();
		this.PaymentList = new ArrayList<Payment>();
		this.PaymentQuery = new ArrayList<Payment>();

		//Error handling for database connection
		if (username != null && password != null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ());
				con = DriverManager.getConnection(db, username, password);
				System.out.println("Database connection successful");
			}
			catch(Exception e) {
				System.out.println("Database connection failed. Error " + e);
			}
		}

	}

	//Checks the connection
	public boolean checkCon() {
		try {
			if (con.isClosed() == true) {
				return false;
			}
		} 
		catch (SQLException e) {
			System.out.println(e);
		}
		return true;

	}

	//Retrieves and stores data from customer table
	public void getCustomer() {
		try {
			String query = "select * from customers";
			Statement statement = con.createStatement();
			ResultSet results = statement.executeQuery(query);

			while (results.next()) {

				/**
				 * This code will assign the results from the SQL query to their own variable
				 */
				int customerNumber = results.getInt("customerNumber");
				String customerName = results.getString("customerName");
				String contactLastName = results.getString("contactLastName");
				String contactFirstName = results.getString("contactFirstName");
				String phone = results.getString("phone");
				String addressLine1 = results.getString("addressLine1");
				String addressLine2 = results.getString("addressLine2");
				String city = results.getString("city");
				String postalCode = results.getString("postalCode");
				String country = results.getString("country");
				int salesRepEmployeeNumber = results.getInt("salesRepEmployeeNumber");
				double creditLimit = results.getDouble("creditLimit");

				Customer tempCustomer = new Customer(customerNumber, customerName, contactLastName, 
						contactFirstName, phone, addressLine1, addressLine2, city, 
						postalCode, country, salesRepEmployeeNumber, creditLimit);
				CustomerList.add(tempCustomer);
			}

		}
		catch (SQLException e) {
			System.out.println("An error occurred while making this query.");
			throw new RuntimeException(e);
		} 

	}

	//Retrieves and stores data from orders table
	public void getOrders() {
		try {
			String query = "select * from orders";
			Statement statement = con.createStatement();
			ResultSet results = statement.executeQuery(query);

			while (results.next()) {

				/**
				 * This code will assign the results from the SQL query to their own variable
				 */
				int orderNumber = results.getInt("orderNumber");
				String orderDate = results.getString("orderDate");
				String requiredDate = results.getString("requiredDate");
				String shippedDate = results.getString("shippedDate");
				String status = results.getString("status");
				String comments = results.getString("comments");
				int customerNumber = results.getInt("customerNumber");

				Orders tempOrder = new Orders(orderNumber, orderDate, requiredDate,
						shippedDate, status, comments, customerNumber);
				OrdersList.add(tempOrder);
			}

		}
		catch (SQLException e) {
			System.out.println("An error occurred while making this query.");
			throw new RuntimeException(e);
		} 

	}

	//Retrieves and stores data from payments table
	public void getPayment() {
		try {
			String query = "select * from payments";
			Statement statement = con.createStatement();
			ResultSet results = statement.executeQuery(query);

			while (results.next()) {

				/**
				 * This code will assign the results from the SQL query to their own variable
				 */
				int customerNumber = results.getInt("customerNumber");
				String checkNumber = results.getString("checkNumber");
				String date = results.getString("paymentDate");
				double amount = results.getDouble("amount");

				Payment tempPayment = new Payment(customerNumber, checkNumber, date, amount);
				PaymentList.add(tempPayment);
			}

		}
		catch (SQLException e) {
			System.out.println("An error occurred while making this query.");
			throw new RuntimeException(e);
		} 

	}

	public void getCustomerQuery() {
		try {
			String query = "select * from customers";
			Statement statement = con.createStatement();
			ResultSet results = statement.executeQuery(query);

			while (results.next()) {

				/**
				 * This code will assign the results from the SQL query to their own variable
				 */
				int customerNumber = results.getInt("customerNumber");
				String customerName = results.getString("customerName");
				String contactLastName = results.getString("contactLastName");
				String contactFirstName = results.getString("contactFirstName");
				String phone = results.getString("phone");
				String addressLine1 = results.getString("addressLine1");
				String addressLine2 = results.getString("addressLine2");
				String city = results.getString("city");
				String postalCode = results.getString("postalCode");
				String country = results.getString("country");
				int salesRepEmployeeNumber = results.getInt("salesRepEmployeeNumber");
				double creditLimit = results.getDouble("creditLimit");

				Customer tempCustomerQuery = new Customer(customerNumber, customerName, contactLastName, 
						contactFirstName, phone, addressLine1, addressLine2, city, 
						postalCode, country, salesRepEmployeeNumber, creditLimit);
				CustomerQuery.add(tempCustomerQuery);
			}

		}
		catch (SQLException e) {
			System.out.println("An error occurred while making this query.");
			throw new RuntimeException(e);
		} 

	}

	public void getOrdersQuery() {
		try {
			String query = "select * from orders";
			Statement statement = con.createStatement();
			ResultSet results = statement.executeQuery(query);

			while (results.next()) {

				/**
				 * This code will assign the results from the SQL query to their own variable
				 */
				int orderNumber = results.getInt("orderNumber");
				String orderDate = results.getString("orderDate");
				String requiredDate = results.getString("requiredDate");
				String shippedDate = results.getString("shippedDate");
				String status = results.getString("status");
				String comments = results.getString("comments");
				int customerNumber = results.getInt("customerNumber");

				Orders tempOrderQuery = new Orders(orderNumber, orderDate, requiredDate,
						shippedDate, status, comments, customerNumber);
				OrdersQuery.add(tempOrderQuery);
			}

		}
		catch (SQLException e) {
			System.out.println("An error occurred while making this query.");
			throw new RuntimeException(e);
		} 

	}

	public void getPaymentQuery() {
		try {
			String query = "select * from payments, customers where (amount > 25000) && (payments.customerNumber = customers.customerNumber)";
			Statement statement = con.createStatement();
			ResultSet results = statement.executeQuery(query);

			while (results.next()) {

				/**
				 * This code will assign the results from the SQL query to their own variable
				 */
				int customerNumber = results.getInt("customerNumber");
				String checkNumber = results.getString("checkNumber");
				String date = results.getString("paymentDate");
				double amount = results.getDouble("amount");

				Payment tempPaymentQuery = new Payment(customerNumber, checkNumber, date, amount);
				PaymentQuery.add(tempPaymentQuery);
			}

		}
		catch (SQLException e) {
			System.out.println("An error occurred while making this query.");
			throw new RuntimeException(e);
		} 

	}

	public String Over25000() {
		StringBuffer Buffer = new StringBuffer();

		for (Payment eachPayment : PaymentList) {
			for(Customer eachCustomer : CustomerList) {
				for(Orders eachOrder : OrdersList) {
					if (eachPayment.getCustomerNumber() == eachCustomer.getCustomerNumber() && 
							eachOrder.getCustomerNumber() == eachCustomer.getCustomerNumber() &&
							eachPayment.getAmount() > 25000) {
						Buffer.append("Customer Name: " + eachCustomer.getCustomerName() + "|" + 
								"Order Number: " + eachOrder.getOrderNumber() + 
								"|" + "Amount: " + eachPayment.getAmount() + "\n");
					}
				}
			}
		}

		return "Customers with orders above $25,000:" + "\n" + Buffer.toString();

	}

	public String Over25000Query() {
		StringBuffer Buffer = new StringBuffer();

		for (Payment eachPayment : PaymentQuery) {
			for(Customer eachCustomer : CustomerQuery) {
				for(Orders eachOrder : OrdersQuery) {
					if (eachPayment.getCustomerNumber() == eachCustomer.getCustomerNumber() && 
							eachOrder.getCustomerNumber() == eachCustomer.getCustomerNumber()) {
						Buffer.append("Customer Name: " + eachCustomer.getCustomerName() + "|" + 
								"Order Number: " + eachOrder.getOrderNumber() + 
								"|" + "Amount: " + eachPayment.getAmount() + "\n");
					}

				}
			}
		}

		return "Customers with orders above $25,000:" + "\n" + Buffer.toString();

	}

}
