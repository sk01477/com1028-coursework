/**
 * Requirement1.java
 */
package com.com1028.assignment;

/**
 * @author Shirshendu Karmakar
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.com1028.assignment.Payment;

public class Requirement1 {

	//Fields set to default values 
	protected Connection con = null;
	private Statement statement = null;
	private ResultSet results = null;
	private List<Payment> PaymentList = null;
	private List<Payment> QueryList = null;
	//db set to connect to database
	private String db = "jdbc:mysql://localhost:3306/classicmodels?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private String username = null;
	private String password = null;

	/**
	 * Constructor for this class
	 * 
	 * @param username for connecting to database
	 * 
	 * @param password for connecting to database
	 */
	public Requirement1(String username, String password) {
		super();
		this.username = username;
		this.password = password;
		this.PaymentList = new ArrayList<Payment>();
		this.QueryList = new ArrayList<Payment>();

		//Error handling for database connection
		if (username != null && password != null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ());
				con = DriverManager.getConnection(db, username, password);
				System.out.println("Database connection successful");
			}
			catch(Exception e) {
				System.out.println("Database connection failed. Error " + e);
			}
		} 

	}

	//Checks the connection
	public boolean checkCon() {
		try {
			if (con.isClosed() == true) {
				return false;
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
		return true;

	}

	//Retrieves and stores data from payments table
	public void getData() {
		try {
			String query = "select * from payments";
			Statement statement = con.createStatement();
			ResultSet results = statement.executeQuery(query);

			while (results.next()) {

				/**
				 * This code will assign the results from the SQL query to their own variable
				 */
				int customerNumber = results.getInt("customerNumber");
				String checkNumber = results.getString("checkNumber");
				String date = results.getString("paymentDate");
				double amount = results.getDouble("amount");

				Payment tempPayment = new Payment(customerNumber, checkNumber, date, amount);
				PaymentList.add(tempPayment);
			}

		}
		catch (SQLException e) {
			System.out.println("An error occurred while making this query.");
			throw new RuntimeException(e);
		} 

	}

	//Executes filtered SQL query to collect list of payments greater than twice the average payment
	public void getAnswers() {
		try {
			String query = "select * from `Payments` where amount > ((select avg(amount) from `Payments`)*2)";
			Statement statement = con.createStatement();
			ResultSet results = statement.executeQuery(query);

			while (results.next()) {
				int customerNumber = results.getInt("customerNumber");
				String checkNumber = results.getString("checkNumber");
				String date = results.getString("paymentDate");
				double amount = results.getDouble("amount");

				Payment tempQuery = new Payment(customerNumber, checkNumber, date, amount);
				QueryList.add(tempQuery);
			}

		} 
		catch (SQLException e) {
			System.out.println("An error occurred while making this query.");
			throw new RuntimeException(e);
		}
	}

	//Outputs all payments greater than twice the average payment through a String Buffer
	public String twiceAverage() {
		double average = 0;
		double total = 0;
		StringBuffer Buffer = new StringBuffer();
		for (Payment eachPayment : PaymentList) {
			total = total + eachPayment.getAmount();
			average = average + 1;
		}

		for (Payment eachPayment : PaymentList) {
			if (eachPayment.getAmount() > (total/average) * 2) {
				Buffer.append("Customer Number: " + eachPayment.getCustomerNumber() + "|" +
						"Check Number: " + eachPayment.getCheckNumber() + "|" + "Payment Date: " + eachPayment.getPaymentdate() +
						"|" + "Amount: $" + eachPayment.getAmount() + "\n");
			}
		}

		return Buffer.toString();

	}

	//Outputs the data from the filtered query through a String Buffer
	public String twiceQuery() {
		StringBuffer Buffer = new StringBuffer();
		for (Payment eachQuery : QueryList) {
			Buffer.append("Customer Number: " + eachQuery.getCustomerNumber() + "|" +
					"Check Number: " + eachQuery.getCheckNumber() + "|" + "Payment Date: " + eachQuery.getPaymentdate() +
					"|" + "Amount: $" + eachQuery.getAmount() + "\n");
		}

		return Buffer.toString();
	}

}
