/**
 * Payment.java
 */
package com.com1028.assignment;

/**
 * @author Shirshendu Karmakar
 */

public class Payment {

	private int customerNumber = 0;
	private String checkNumber = null;
	private String paymentdate = null;
	private double amount = 0;

	public Payment(int customerNumber, String checkNumber, String paymentdate, double amount) {
		super();
		this.customerNumber = customerNumber;
		this.checkNumber = checkNumber;
		this.paymentdate = paymentdate;
		this.amount = amount;
	}

	public int getCustomerNumber() {
		return customerNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public String getPaymentdate() {
		return paymentdate;
	}

	public double getAmount() {
		return amount;
	}

}