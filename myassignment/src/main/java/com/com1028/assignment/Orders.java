/**
 * Orders.java 
 */
package com.com1028.assignment;

/**
 * @author Shirshendu Karmakar
 */

public class Orders {

	private int orderNumber = 0;
	private String orderDate = null;
	private String requiredDate = null;
	private String shippedDate = null;
	private String status = null;
	private String comments = null;
	private int customerNumber = 0;

	public Orders(int orderNumber, String orderDate, String requiredDate, 
			String shippedDate, String status, String comments, int customerNumber) {
		super();
		this.orderNumber = orderNumber;
		this.orderDate = orderDate;
		this.requiredDate = requiredDate;
		this.shippedDate = shippedDate;
		this.status = status;
		this.comments = comments;
		this.customerNumber = customerNumber;

	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public String getRequiredDate() {
		return requiredDate;
	}

	public String getShippedDate() {
		return shippedDate;
	}

	public String getStatus() {
		return status;
	}

	public String getComments() {
		return comments;
	}

	public int getCustomerNumber() {
		return customerNumber;
	}

}
