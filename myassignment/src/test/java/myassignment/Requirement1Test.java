/**
 * Requirement1Test.java 
 */
package myassignment;

/**
 * Test for Requirement1.java
 * 
 * @author Shirshendu Karmakar
 */

import static org.junit.Assert.*;
import org.junit.Test;

import com.com1028.assignment.Requirement1;

public class Requirement1Test {

	@Test 
	public void testcheckCon() {
		Requirement1 testObject = new Requirement1("root", "");
		assertEquals(true, testObject.checkCon());
	}

	@Test (expected=NullPointerException.class)
	public void testcheckCon2() {
		Requirement1 testObject = new Requirement1("username", "password");
		assertEquals(false, testObject.checkCon());
	}

	@Test (expected=NullPointerException.class)
	public void testcheckCon3() {
		Requirement1 testObject = new Requirement1("", "");
		assertEquals(false, testObject.checkCon());
	}

	@Test
	public void testtwiceAverage() {
		Requirement1 testObject = new Requirement1("root", "");

		testObject.getData();

		assertEquals("Customer Number: 114|Check Number: MA765515|Payment Date: 2004-12-15|Amount: $82261.22" + "\n" +
				"Customer Number: 124|Check Number: AE215433|Payment Date: 2005-03-05|Amount: $101244.59" + "\n" +
				"Customer Number: 124|Check Number: BG255406|Payment Date: 2004-08-28|Amount: $85410.87" + "\n" +
				"Customer Number: 124|Check Number: ET64396|Payment Date: 2005-04-16|Amount: $83598.04" + "\n" +
				"Customer Number: 124|Check Number: KI131716|Payment Date: 2003-08-15|Amount: $111654.4" + "\n" +
				"Customer Number: 141|Check Number: ID10962|Payment Date: 2004-12-31|Amount: $116208.4" + "\n" +
				"Customer Number: 141|Check Number: IN446258|Payment Date: 2005-03-25|Amount: $65071.26" + "\n" +
				"Customer Number: 141|Check Number: JE105477|Payment Date: 2005-03-18|Amount: $120166.58" + "\n" +
				"Customer Number: 148|Check Number: KM172879|Payment Date: 2003-12-26|Amount: $105743.0" + "\n" +
				"Customer Number: 167|Check Number: GN228846|Payment Date: 2003-12-03|Amount: $85024.46" + "\n" +
				"Customer Number: 239|Check Number: NQ865547|Payment Date: 2004-03-15|Amount: $80375.24" + "\n" +
				"Customer Number: 321|Check Number: DJ15149|Payment Date: 2003-11-03|Amount: $85559.12" + "\n" +
				"Customer Number: 323|Check Number: AL493079|Payment Date: 2005-05-23|Amount: $75020.13" + "\n", 
				testObject.twiceAverage());

	}

	@Test
	public void testtwiceQuery() {
		Requirement1 testObject = new Requirement1("root", "");

		testObject.getAnswers();

		assertEquals("Customer Number: 114|Check Number: MA765515|Payment Date: 2004-12-15|Amount: $82261.22" + "\n" +
				"Customer Number: 124|Check Number: AE215433|Payment Date: 2005-03-05|Amount: $101244.59" + "\n" +
				"Customer Number: 124|Check Number: BG255406|Payment Date: 2004-08-28|Amount: $85410.87" + "\n" +
				"Customer Number: 124|Check Number: ET64396|Payment Date: 2005-04-16|Amount: $83598.04" + "\n" +
				"Customer Number: 124|Check Number: KI131716|Payment Date: 2003-08-15|Amount: $111654.4" + "\n" +
				"Customer Number: 141|Check Number: ID10962|Payment Date: 2004-12-31|Amount: $116208.4" + "\n" +
				"Customer Number: 141|Check Number: IN446258|Payment Date: 2005-03-25|Amount: $65071.26" + "\n" +
				"Customer Number: 141|Check Number: JE105477|Payment Date: 2005-03-18|Amount: $120166.58" + "\n" +
				"Customer Number: 148|Check Number: KM172879|Payment Date: 2003-12-26|Amount: $105743.0" + "\n" +
				"Customer Number: 167|Check Number: GN228846|Payment Date: 2003-12-03|Amount: $85024.46" + "\n" +
				"Customer Number: 239|Check Number: NQ865547|Payment Date: 2004-03-15|Amount: $80375.24" + "\n" +
				"Customer Number: 321|Check Number: DJ15149|Payment Date: 2003-11-03|Amount: $85559.12" + "\n" +
				"Customer Number: 323|Check Number: AL493079|Payment Date: 2005-05-23|Amount: $75020.13" + "\n", 
				testObject.twiceQuery());
	}

	@Test
	public void compareAverage() {
		Requirement1 testObject = new Requirement1("root", "");
		Requirement1 testObject2 = new Requirement1("root", "");

		testObject.getData();
		testObject2.getAnswers();

		assertEquals(testObject.twiceAverage(), testObject2.twiceQuery());

	}

}