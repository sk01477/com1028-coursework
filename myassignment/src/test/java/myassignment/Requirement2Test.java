/**
 * Requirement2Test.java 
 */
package myassignment;

/**
 * Test for Requirement2.java
 * 
 * @author Shirshendu Karmakar
 */

import static org.junit.Assert.*;
import org.junit.Test;

import com.com1028.assignment.Requirement2;

public class Requirement2Test {

	@Test 
	public void testcheckCon() {
		Requirement2 testObject = new Requirement2("root", "");
		assertEquals(true, testObject.checkCon());
	}

	@Test (expected=NullPointerException.class)
	public void testcheckCon2() {
		Requirement2 testObject = new Requirement2("username", "password");
		assertEquals(false, testObject.checkCon());
	}

	@Test (expected=NullPointerException.class)
	public void testcheckCon3() {
		Requirement2 testObject = new Requirement2("", "");
		assertEquals(false, testObject.checkCon());
	}

	@Test
	public void testinBoston() {
		Requirement2 testEmployee = new Requirement2("root", "");

		testEmployee.getEmployees();

		assertEquals("Employees in Boston:" + "\n" + 
				"Employee Number: 1188|Last Name: Firrelli|First Name: Julie\n" + 
				"Extension: x2173|Email: jfirrelli@classicmodelcars.com" + "\n" + 
				"Office Code: 2|Reports To: 1143|Job Title: Sales Rep" + "\n" + 
				"Employee Number: 1216|Last Name: Patterson|First Name: Steve" + "\n" + 
				"Extension: x4334|Email: spatterson@classicmodelcars.com" + "\n" + 
				"Office Code: 2|Reports To: 1143|Job Title: Sales Rep" + "\n", 
				testEmployee.inBoston());

	}

	@Test
	public void testinBostonQuery() {
		Requirement2 testEmployee = new Requirement2("root", "");

		testEmployee.getEmployeeQuery();;

		assertEquals("Employees in Boston:" + "\n" + 
				"Employee Number: 1188|Last Name: Firrelli|First Name: Julie\n" + 
				"Extension: x2173|Email: jfirrelli@classicmodelcars.com" + "\n" + 
				"Office Code: 2|Reports To: 1143|Job Title: Sales Rep" + "\n" + 
				"Employee Number: 1216|Last Name: Patterson|First Name: Steve" + "\n" + 
				"Extension: x4334|Email: spatterson@classicmodelcars.com" + "\n" + 
				"Office Code: 2|Reports To: 1143|Job Title: Sales Rep" + "\n", 
				testEmployee.inBostonQuery());

	}

	@Test
	public void testcompareBoston() {
		Requirement2 testEmployee = new Requirement2("root", "");
		Requirement2 testEmployee2 = new Requirement2("root", "");

		testEmployee.getEmployees();
		testEmployee2.getEmployeeQuery();

		assertEquals(testEmployee.inBoston(), testEmployee2.inBostonQuery());

	}

}
