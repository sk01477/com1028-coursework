/**
 * Requirement3Test.java 
 */
package myassignment;

/**
 * Test for Requirement3.java
 * 
 * @author Shirshendu Karmakar
 */

import static org.junit.Assert.*;
import org.junit.Test;

import com.com1028.assignment.Requirement3;

public class Requirement3Test {

	@Test 
	public void testcheckCon() {
		Requirement3 testObject = new Requirement3("root", "");
		assertEquals(true, testObject.checkCon());
	}

	@Test (expected=NullPointerException.class)
	public void testcheckCon2() {
		Requirement3 testObject = new Requirement3("username", "password");
		assertEquals(false, testObject.checkCon());
	}

	@Test (expected=NullPointerException.class)
	public void testcheckCon3() {
		Requirement3 testObject = new Requirement3("", "");
		assertEquals(false, testObject.checkCon());
	}

	@Test //Must fix java.lang.OutofMemoryError otherwise test will fail 
	public void testCompare25k() {
		Requirement3 test25k = new Requirement3("root", "");
		Requirement3 test25kQuery = new Requirement3("root", "");

		test25k.getCustomer();
		test25k.getOrders();
		test25k.getPayment();
		test25kQuery.getCustomerQuery();
		test25kQuery.getOrdersQuery();
		test25kQuery.getPaymentQuery();

		assertEquals(test25k.Over25000(), test25kQuery.Over25000Query());

	}

	@Test
	public void testOver25k() {
		Requirement3 test25k = new Requirement3("root", "");

		test25k.getCustomer();
		test25k.getOrders();
		test25k.getPayment();

		assertEquals("Customers with orders above $25,000:" + "\n" + 
				"Customer Name: Signal Gift Stores|Order Number: 10124|Amount: 32641.98" + "\n" + 
				"Customer Name: Signal Gift Stores|Order Number: 10278|Amount: 32641.98" + "\n" + 
				"Customer Name: Signal Gift Stores|Order Number: 10346|Amount: 32641.98" + "\n" + 
				"Customer Name: Signal Gift Stores|Order Number: 10124|Amount: 33347.88" + "\n" + 
				"Customer Name: Signal Gift Stores|Order Number: 10278|Amount: 33347.88" + "\n" + 
				"Customer Name: Signal Gift Stores|Order Number: 10346|Amount: 33347.88" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10120|Amount: 45864.03" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10125|Amount: 45864.03" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10223|Amount: 45864.03" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10342|Amount: 45864.03" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10347|Amount: 45864.03" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10120|Amount: 82261.22" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10125|Amount: 82261.22" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10223|Amount: 82261.22" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10342|Amount: 82261.22" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10347|Amount: 82261.22" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10120|Amount: 44894.74" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10125|Amount: 44894.74" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10223|Amount: 44894.74" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10342|Amount: 44894.74" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10347|Amount: 44894.74" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10275|Amount: 47924.19" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10315|Amount: 47924.19" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10375|Amount: 47924.19" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10425|Amount: 47924.19" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10275|Amount: 49523.67" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10315|Amount: 49523.67" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10375|Amount: 49523.67" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10425|Amount: 49523.67" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10103|Amount: 50218.95" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10158|Amount: 50218.95" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10309|Amount: 50218.95" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10325|Amount: 50218.95" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10103|Amount: 34638.14" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10158|Amount: 34638.14" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10309|Amount: 34638.14" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10325|Amount: 34638.14" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 45084.38" + "\n" + 
				"Customer Name: Blauer See Auto, Co.|Order Number: 10101|Amount: 33820.62" + "\n" + 
				"Customer Name: Blauer See Auto, Co.|Order Number: 10230|Amount: 33820.62" + "\n" + 
				"Customer Name: Blauer See Auto, Co.|Order Number: 10300|Amount: 33820.62" + "\n" + 
				"Customer Name: Blauer See Auto, Co.|Order Number: 10323|Amount: 33820.62" + "\n" + 
				"Customer Name: Mini Wheels Co.|Order Number: 10111|Amount: 26248.78" + "\n" + 
				"Customer Name: Mini Wheels Co.|Order Number: 10201|Amount: 26248.78" + "\n" + 
				"Customer Name: Mini Wheels Co.|Order Number: 10333|Amount: 26248.78" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10107|Amount: 50025.35" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10248|Amount: 50025.35" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10292|Amount: 50025.35" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10329|Amount: 50025.35" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10107|Amount: 35321.97" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10248|Amount: 35321.97" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10292|Amount: 35321.97" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10329|Amount: 35321.97" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 26155.91" + "\n" + 
				"Customer Name: Volvo Model Replicas, Co|Order Number: 10112|Amount: 36005.71" + "\n" + 
				"Customer Name: Volvo Model Replicas, Co|Order Number: 10320|Amount: 36005.71" + "\n" + 
				"Customer Name: Volvo Model Replicas, Co|Order Number: 10326|Amount: 36005.71" + "\n" + 
				"Customer Name: Volvo Model Replicas, Co|Order Number: 10334|Amount: 36005.71" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10105|Amount: 28211.7" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10238|Amount: 28211.7" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10256|Amount: 28211.7" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10327|Amount: 28211.7" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10406|Amount: 28211.7" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10105|Amount: 53959.21" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10238|Amount: 53959.21" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10256|Amount: 53959.21" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10327|Amount: 53959.21" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10406|Amount: 53959.21" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10194|Amount: 40978.53" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10208|Amount: 40978.53" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10227|Amount: 40978.53" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10194|Amount: 49614.72" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10208|Amount: 49614.72" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10227|Amount: 49614.72" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10194|Amount: 39712.1" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10208|Amount: 39712.1" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10227|Amount: 39712.1" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10117|Amount: 44380.15" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10150|Amount: 44380.15" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10165|Amount: 44380.15" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10277|Amount: 44380.15" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10387|Amount: 44380.15" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10117|Amount: 105743.0" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10150|Amount: 105743.0" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10165|Amount: 105743.0" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10277|Amount: 105743.0" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10387|Amount: 105743.0" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10127|Amount: 58793.53" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10204|Amount: 58793.53" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10267|Amount: 58793.53" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10349|Amount: 58793.53" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10127|Amount: 58841.35" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10204|Amount: 58841.35" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10267|Amount: 58841.35" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10349|Amount: 58841.35" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10127|Amount: 39964.63" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10204|Amount: 39964.63" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10267|Amount: 39964.63" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10349|Amount: 39964.63" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10272|Amount: 35152.12" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10281|Amount: 35152.12" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10318|Amount: 35152.12" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10422|Amount: 35152.12" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10272|Amount: 63357.13" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10281|Amount: 63357.13" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10318|Amount: 63357.13" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10422|Amount: 63357.13" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10140|Amount: 50743.65" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10168|Amount: 50743.65" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10317|Amount: 50743.65" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10362|Amount: 50743.65" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10140|Amount: 38675.13" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10168|Amount: 38675.13" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10317|Amount: 38675.13" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10362|Amount: 38675.13" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10217|Amount: 38785.48" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10259|Amount: 38785.48" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10288|Amount: 38785.48" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10409|Amount: 38785.48" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10217|Amount: 44160.92" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10259|Amount: 44160.92" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10288|Amount: 44160.92" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10409|Amount: 44160.92" + "\n" + 
				"Customer Name: Herkku Gifts|Order Number: 10181|Amount: 85024.46" + "\n" + 
				"Customer Name: Herkku Gifts|Order Number: 10188|Amount: 85024.46" + "\n" + 
				"Customer Name: Herkku Gifts|Order Number: 10289|Amount: 85024.46" + "\n" + 
				"Customer Name: Daedalus Designs Imports|Order Number: 10180|Amount: 42783.81" + "\n" + 
				"Customer Name: Daedalus Designs Imports|Order Number: 10224|Amount: 42783.81" + "\n" + 
				"Customer Name: La Corne D'abondance, Co.|Order Number: 10114|Amount: 51209.58" + "\n" + 
				"Customer Name: La Corne D'abondance, Co.|Order Number: 10286|Amount: 51209.58" + "\n" + 
				"Customer Name: La Corne D'abondance, Co.|Order Number: 10336|Amount: 51209.58" + "\n" + 
				"Customer Name: La Corne D'abondance, Co.|Order Number: 10114|Amount: 33383.14" + "\n" + 
				"Customer Name: La Corne D'abondance, Co.|Order Number: 10286|Amount: 33383.14" + "\n" + 
				"Customer Name: La Corne D'abondance, Co.|Order Number: 10336|Amount: 33383.14" + "\n" + 
				"Customer Name: Gift Depot Inc.|Order Number: 10172|Amount: 28500.78" + "\n" + 
				"Customer Name: Gift Depot Inc.|Order Number: 10263|Amount: 28500.78" + "\n" + 
				"Customer Name: Gift Depot Inc.|Order Number: 10413|Amount: 28500.78" + "\n" + 
				"Customer Name: Gift Depot Inc.|Order Number: 10172|Amount: 42044.77" + "\n" + 
				"Customer Name: Gift Depot Inc.|Order Number: 10263|Amount: 42044.77" + "\n" + 
				"Customer Name: Gift Depot Inc.|Order Number: 10413|Amount: 42044.77" + "\n" + 
				"Customer Name: Osaka Souveniers Co.|Order Number: 10210|Amount: 47177.59" + "\n" + 
				"Customer Name: Osaka Souveniers Co.|Order Number: 10240|Amount: 47177.59" + "\n" + 
				"Customer Name: Vitachrome Inc.|Order Number: 10102|Amount: 44400.5" + "\n" + 
				"Customer Name: Vitachrome Inc.|Order Number: 10237|Amount: 44400.5" + "\n" + 
				"Customer Name: Vitachrome Inc.|Order Number: 10324|Amount: 44400.5" + "\n" + 
				"Customer Name: Toys of Finland, Co.|Order Number: 10155|Amount: 37602.48" + "\n" + 
				"Customer Name: Toys of Finland, Co.|Order Number: 10299|Amount: 37602.48" + "\n" + 
				"Customer Name: Toys of Finland, Co.|Order Number: 10377|Amount: 37602.48" + "\n" + 
				"Customer Name: Toys of Finland, Co.|Order Number: 10155|Amount: 34341.08" + "\n" + 
				"Customer Name: Toys of Finland, Co.|Order Number: 10299|Amount: 34341.08" + "\n" + 
				"Customer Name: Toys of Finland, Co.|Order Number: 10377|Amount: 34341.08" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10110|Amount: 52825.29" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10306|Amount: 52825.29" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10332|Amount: 52825.29" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10110|Amount: 47159.11" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10306|Amount: 47159.11" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10332|Amount: 47159.11" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10110|Amount: 48425.69" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10306|Amount: 48425.69" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10332|Amount: 48425.69" + "\n" + 
				"Customer Name: Clover Collections, Co.|Order Number: 10220|Amount: 32538.74" + "\n" + 
				"Customer Name: Clover Collections, Co.|Order Number: 10297|Amount: 32538.74" + "\n" + 
				"Customer Name: UK Collectables, Ltd.|Order Number: 10253|Amount: 37258.94" + "\n" + 
				"Customer Name: UK Collectables, Ltd.|Order Number: 10302|Amount: 37258.94" + "\n" + 
				"Customer Name: UK Collectables, Ltd.|Order Number: 10403|Amount: 37258.94" + "\n" + 
				"Customer Name: Canadian Gift Exchange Network|Order Number: 10206|Amount: 36527.61" + "\n" + 
				"Customer Name: Canadian Gift Exchange Network|Order Number: 10313|Amount: 36527.61" + "\n" + 
				"Customer Name: Canadian Gift Exchange Network|Order Number: 10206|Amount: 33594.58" + "\n" + 
				"Customer Name: Canadian Gift Exchange Network|Order Number: 10313|Amount: 33594.58" + "\n" + 
				"Customer Name: Online Mini Collectables|Order Number: 10276|Amount: 51152.86" + "\n" + 
				"Customer Name: Online Mini Collectables|Order Number: 10294|Amount: 51152.86" + "\n" + 
				"Customer Name: Toys4GrownUps.com|Order Number: 10145|Amount: 50342.74" + "\n" + 
				"Customer Name: Toys4GrownUps.com|Order Number: 10189|Amount: 50342.74" + "\n" + 
				"Customer Name: Toys4GrownUps.com|Order Number: 10367|Amount: 50342.74" + "\n" + 
				"Customer Name: Toys4GrownUps.com|Order Number: 10145|Amount: 39580.6" + "\n" + 
				"Customer Name: Toys4GrownUps.com|Order Number: 10189|Amount: 39580.6" + "\n" + 
				"Customer Name: Toys4GrownUps.com|Order Number: 10367|Amount: 39580.6" + "\n" + 
				"Customer Name: Mini Caravy|Order Number: 10241|Amount: 35157.75" + "\n" + 
				"Customer Name: Mini Caravy|Order Number: 10255|Amount: 35157.75" + "\n" + 
				"Customer Name: Mini Caravy|Order Number: 10405|Amount: 35157.75" + "\n" + 
				"Customer Name: Mini Caravy|Order Number: 10241|Amount: 36069.26" + "\n" + 
				"Customer Name: Mini Caravy|Order Number: 10255|Amount: 36069.26" + "\n" + 
				"Customer Name: Mini Caravy|Order Number: 10405|Amount: 36069.26" + "\n" + 
				"Customer Name: King Kong Collectables, Co.|Order Number: 10187|Amount: 45480.79" + "\n" + 
				"Customer Name: King Kong Collectables, Co.|Order Number: 10200|Amount: 45480.79" + "\n" + 
				"Customer Name: Enaco Distributors|Order Number: 10118|Amount: 40473.86" + "\n" + 
				"Customer Name: Enaco Distributors|Order Number: 10197|Amount: 40473.86" + "\n" + 
				"Customer Name: Enaco Distributors|Order Number: 10340|Amount: 40473.86" + "\n" + 
				"Customer Name: Heintze Collectables|Order Number: 10161|Amount: 36164.46" + "\n" + 
				"Customer Name: Heintze Collectables|Order Number: 10314|Amount: 36164.46" + "\n" + 
				"Customer Name: Heintze Collectables|Order Number: 10161|Amount: 53745.34" + "\n" + 
				"Customer Name: Heintze Collectables|Order Number: 10314|Amount: 53745.34" + "\n" + 
				"Customer Name: Qu�bec Home Shopping Network|Order Number: 10171|Amount: 29070.38" + "\n" + 
				"Customer Name: Qu�bec Home Shopping Network|Order Number: 10261|Amount: 29070.38" + "\n" + 
				"Customer Name: Qu�bec Home Shopping Network|Order Number: 10411|Amount: 29070.38" + "\n" + 
				"Customer Name: Collectable Mini Designs Co.|Order Number: 10222|Amount: 80375.24" + "\n" + 
				"Customer Name: Collectable Mini Designs Co.|Order Number: 10226|Amount: 80375.24" + "\n" + 
				"Customer Name: giftsbymail.co.uk|Order Number: 10232|Amount: 46788.14" + "\n" + 
				"Customer Name: giftsbymail.co.uk|Order Number: 10316|Amount: 46788.14" + "\n" + 
				"Customer Name: Alpha Cognac|Order Number: 10136|Amount: 33818.34" + "\n" + 
				"Customer Name: Alpha Cognac|Order Number: 10178|Amount: 33818.34" + "\n" + 
				"Customer Name: Alpha Cognac|Order Number: 10397|Amount: 33818.34" + "\n" + 
				"Customer Name: Amica Models & Co.|Order Number: 10280|Amount: 33924.24" + "\n" + 
				"Customer Name: Amica Models & Co.|Order Number: 10293|Amount: 33924.24" + "\n" + 
				"Customer Name: Amica Models & Co.|Order Number: 10280|Amount: 48298.99" + "\n" + 
				"Customer Name: Amica Models & Co.|Order Number: 10293|Amount: 48298.99" + "\n" + 
				"Customer Name: Lyon Souveniers|Order Number: 10134|Amount: 26311.63" + "\n" + 
				"Customer Name: Lyon Souveniers|Order Number: 10356|Amount: 26311.63" + "\n" + 
				"Customer Name: Lyon Souveniers|Order Number: 10395|Amount: 26311.63" + "\n" + 
				"Customer Name: Auto Associ�s & Cie.|Order Number: 10216|Amount: 53116.99" + "\n" + 
				"Customer Name: Auto Associ�s & Cie.|Order Number: 10304|Amount: 53116.99" + "\n" + 
				"Customer Name: Toms Spezialit�ten, Ltd|Order Number: 10191|Amount: 61234.67" + "\n" + 
				"Customer Name: Toms Spezialit�ten, Ltd|Order Number: 10310|Amount: 61234.67" + "\n" + 
				"Customer Name: Toms Spezialit�ten, Ltd|Order Number: 10191|Amount: 27988.47" + "\n" + 
				"Customer Name: Toms Spezialit�ten, Ltd|Order Number: 10310|Amount: 27988.47" + "\n" + 
				"Customer Name: Royal Canadian Collectables, Ltd.|Order Number: 10235|Amount: 37527.58" + "\n" + 
				"Customer Name: Royal Canadian Collectables, Ltd.|Order Number: 10283|Amount: 37527.58" + "\n" + 
				"Customer Name: Royal Canadian Collectables, Ltd.|Order Number: 10235|Amount: 29284.42" + "\n" + 
				"Customer Name: Royal Canadian Collectables, Ltd.|Order Number: 10283|Amount: 29284.42" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10148|Amount: 27083.78" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10169|Amount: 27083.78" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10370|Amount: 27083.78" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10391|Amount: 27083.78" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10148|Amount: 38547.19" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10169|Amount: 38547.19" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10370|Amount: 38547.19" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10391|Amount: 38547.19" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10148|Amount: 41554.73" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10169|Amount: 41554.73" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10370|Amount: 41554.73" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10391|Amount: 41554.73" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10148|Amount: 29848.52" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10169|Amount: 29848.52" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10370|Amount: 29848.52" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10391|Amount: 29848.52" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10106|Amount: 37654.09" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10173|Amount: 37654.09" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10328|Amount: 37654.09" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10106|Amount: 52151.81" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10173|Amount: 52151.81" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10328|Amount: 52151.81" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10106|Amount: 37723.79" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10173|Amount: 37723.79" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10328|Amount: 37723.79" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10139|Amount: 35806.73" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10270|Amount: 35806.73" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10361|Amount: 35806.73" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10420|Amount: 35806.73" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10139|Amount: 31835.36" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10270|Amount: 31835.36" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10361|Amount: 31835.36" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10420|Amount: 31835.36" + "\n" + 
				"Customer Name: Marta's Replicas Co.|Order Number: 10285|Amount: 47411.33" + "\n" + 
				"Customer Name: Marta's Replicas Co.|Order Number: 10305|Amount: 47411.33" + "\n" + 
				"Customer Name: Marta's Replicas Co.|Order Number: 10285|Amount: 43134.04" + "\n" + 
				"Customer Name: Marta's Replicas Co.|Order Number: 10305|Amount: 43134.04" + "\n" + 
				"Customer Name: Vida Sport, Ltd|Order Number: 10225|Amount: 47375.92" + "\n" + 
				"Customer Name: Vida Sport, Ltd|Order Number: 10287|Amount: 47375.92" + "\n" + 
				"Customer Name: Vida Sport, Ltd|Order Number: 10225|Amount: 61402.0" + "\n" + 
				"Customer Name: Vida Sport, Ltd|Order Number: 10287|Amount: 61402.0" + "\n" + 
				"Customer Name: Norway Gifts By Mail, Co.|Order Number: 10284|Amount: 36798.88" + "\n" + 
				"Customer Name: Norway Gifts By Mail, Co.|Order Number: 10301|Amount: 36798.88" + "\n" + 
				"Customer Name: Norway Gifts By Mail, Co.|Order Number: 10284|Amount: 32260.16" + "\n" + 
				"Customer Name: Norway Gifts By Mail, Co.|Order Number: 10301|Amount: 32260.16" + "\n" + 
				"Customer Name: Oulu Toy Supplies, Inc.|Order Number: 10151|Amount: 46770.52" + "\n" + 
				"Customer Name: Oulu Toy Supplies, Inc.|Order Number: 10239|Amount: 46770.52" + "\n" + 
				"Customer Name: Oulu Toy Supplies, Inc.|Order Number: 10373|Amount: 46770.52" + "\n" + 
				"Customer Name: Oulu Toy Supplies, Inc.|Order Number: 10151|Amount: 32723.04" + "\n" + 
				"Customer Name: Oulu Toy Supplies, Inc.|Order Number: 10239|Amount: 32723.04" + "\n" + 
				"Customer Name: Oulu Toy Supplies, Inc.|Order Number: 10373|Amount: 32723.04" + "\n" + 
				"Customer Name: Petit Auto|Order Number: 10221|Amount: 45352.47" + "\n" + 
				"Customer Name: Petit Auto|Order Number: 10273|Amount: 45352.47" + "\n" + 
				"Customer Name: Petit Auto|Order Number: 10423|Amount: 45352.47" + "\n" + 
				"Customer Name: Mini Classics|Order Number: 10195|Amount: 42339.76" + "\n" + 
				"Customer Name: Mini Classics|Order Number: 10308|Amount: 42339.76" + "\n" + 
				"Customer Name: Mini Classics|Order Number: 10195|Amount: 36092.4" + "\n" + 
				"Customer Name: Mini Classics|Order Number: 10308|Amount: 36092.4" + "\n" + 
				"Customer Name: Mini Creations Ltd.|Order Number: 10143|Amount: 41016.75" + "\n" + 
				"Customer Name: Mini Creations Ltd.|Order Number: 10185|Amount: 41016.75" + "\n" + 
				"Customer Name: Mini Creations Ltd.|Order Number: 10365|Amount: 41016.75" + "\n" + 
				"Customer Name: Mini Creations Ltd.|Order Number: 10143|Amount: 52548.49" + "\n" + 
				"Customer Name: Mini Creations Ltd.|Order Number: 10185|Amount: 52548.49" + "\n" + 
				"Customer Name: Mini Creations Ltd.|Order Number: 10365|Amount: 52548.49" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10159|Amount: 85559.12" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10162|Amount: 85559.12" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10381|Amount: 85559.12" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10384|Amount: 85559.12" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10159|Amount: 46781.66" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10162|Amount: 46781.66" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10381|Amount: 46781.66" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10384|Amount: 46781.66" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10132|Amount: 75020.13" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10254|Amount: 75020.13" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10354|Amount: 75020.13" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10393|Amount: 75020.13" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10404|Amount: 75020.13" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10132|Amount: 37281.36" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10254|Amount: 37281.36" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10354|Amount: 37281.36" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10393|Amount: 37281.36" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10404|Amount: 37281.36" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10132|Amount: 39440.59" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10254|Amount: 39440.59" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10354|Amount: 39440.59" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10393|Amount: 39440.59" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10404|Amount: 39440.59" + "\n" + 
				"Customer Name: Stylish Desk Decors, Co.|Order Number: 10129|Amount: 29429.14" + "\n" + 
				"Customer Name: Stylish Desk Decors, Co.|Order Number: 10175|Amount: 29429.14" + "\n" + 
				"Customer Name: Stylish Desk Decors, Co.|Order Number: 10351|Amount: 29429.14" + "\n" + 
				"Customer Name: Stylish Desk Decors, Co.|Order Number: 10129|Amount: 37455.77" + "\n" + 
				"Customer Name: Stylish Desk Decors, Co.|Order Number: 10175|Amount: 37455.77" + "\n" + 
				"Customer Name: Stylish Desk Decors, Co.|Order Number: 10351|Amount: 37455.77" + "\n" + 
				"Customer Name: Tekni Collectables Inc.|Order Number: 10233|Amount: 31102.85" + "\n" + 
				"Customer Name: Tekni Collectables Inc.|Order Number: 10251|Amount: 31102.85" + "\n" + 
				"Customer Name: Tekni Collectables Inc.|Order Number: 10401|Amount: 31102.85" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10141|Amount: 45785.34" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10247|Amount: 45785.34" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10363|Amount: 45785.34" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10141|Amount: 29716.86" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10247|Amount: 29716.86" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10363|Amount: 29716.86" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10141|Amount: 28394.54" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10247|Amount: 28394.54" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10363|Amount: 28394.54" + "\n" + 
				"Customer Name: Classic Gift Ideas, Inc|Order Number: 10183|Amount: 34606.28" + "\n" + 
				"Customer Name: Classic Gift Ideas, Inc|Order Number: 10307|Amount: 34606.28" + "\n" + 
				"Customer Name: CAF Imports|Order Number: 10177|Amount: 31428.21" + "\n" + 
				"Customer Name: CAF Imports|Order Number: 10231|Amount: 31428.21" + "\n" + 
				"Customer Name: Marseille Mini Autos|Order Number: 10122|Amount: 50824.66" + "\n" + 
				"Customer Name: Marseille Mini Autos|Order Number: 10344|Amount: 50824.66" + "\n" + 
				"Customer Name: Marseille Mini Autos|Order Number: 10364|Amount: 50824.66" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10121|Amount: 49705.52" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10137|Amount: 49705.52" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10343|Amount: 49705.52" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10359|Amount: 49705.52" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10398|Amount: 49705.52" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10121|Amount: 46656.94" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10137|Amount: 46656.94" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10343|Amount: 46656.94" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10359|Amount: 46656.94" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10398|Amount: 46656.94" + "\n" + 
				"Customer Name: GiftsForHim.com|Order Number: 10202|Amount: 36442.34" + "\n" + 
				"Customer Name: GiftsForHim.com|Order Number: 10260|Amount: 36442.34" + "\n" + 
				"Customer Name: GiftsForHim.com|Order Number: 10410|Amount: 36442.34" + "\n" + 
				"Customer Name: Online Diecast Creations Co.|Order Number: 10100|Amount: 50799.69" + "\n" + 
				"Customer Name: Online Diecast Creations Co.|Order Number: 10192|Amount: 50799.69" + "\n" + 
				"Customer Name: Online Diecast Creations Co.|Order Number: 10322|Amount: 50799.69" + "\n" + 
				"Customer Name: Online Diecast Creations Co.|Order Number: 10100|Amount: 55425.77" + "\n" + 
				"Customer Name: Online Diecast Creations Co.|Order Number: 10192|Amount: 55425.77" + "\n" + 
				"Customer Name: Online Diecast Creations Co.|Order Number: 10322|Amount: 55425.77" + "\n" + 
				"Customer Name: Collectables For Less Inc.|Order Number: 10147|Amount: 28322.83" + "\n" + 
				"Customer Name: Collectables For Less Inc.|Order Number: 10274|Amount: 28322.83" + "\n" + 
				"Customer Name: Collectables For Less Inc.|Order Number: 10369|Amount: 28322.83" + "\n" + 
				"Customer Name: Collectables For Less Inc.|Order Number: 10147|Amount: 32680.31" + "\n" + 
				"Customer Name: Collectables For Less Inc.|Order Number: 10274|Amount: 32680.31" + "\n" + 
				"Customer Name: Collectables For Less Inc.|Order Number: 10369|Amount: 32680.31" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10119|Amount: 35826.33" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10269|Amount: 35826.33" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10341|Amount: 35826.33" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10419|Amount: 35826.33" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10119|Amount: 42813.83" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10269|Amount: 42813.83" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10341|Amount: 42813.83" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10419|Amount: 42813.83" + "\n" + 
				"Customer Name: Cruz & Sons Co.|Order Number: 10108|Amount: 51001.22" + "\n" + 
				"Customer Name: Cruz & Sons Co.|Order Number: 10198|Amount: 51001.22" + "\n" + 
				"Customer Name: Cruz & Sons Co.|Order Number: 10330|Amount: 51001.22" + "\n" + 
				"Customer Name: L'ordine Souveniers|Order Number: 10176|Amount: 38524.29" + "\n" + 
				"Customer Name: L'ordine Souveniers|Order Number: 10266|Amount: 38524.29" + "\n" + 
				"Customer Name: L'ordine Souveniers|Order Number: 10416|Amount: 38524.29" + "\n" + 
				"Customer Name: L'ordine Souveniers|Order Number: 10176|Amount: 51619.02" + "\n" + 
				"Customer Name: L'ordine Souveniers|Order Number: 10266|Amount: 51619.02" + "\n" + 
				"Customer Name: L'ordine Souveniers|Order Number: 10416|Amount: 51619.02" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10258|Amount: 33967.73" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10339|Amount: 33967.73" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10372|Amount: 33967.73" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10408|Amount: 33967.73" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10258|Amount: 48927.64" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10339|Amount: 48927.64" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10372|Amount: 48927.64" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10408|Amount: 48927.64" + "\n" + 
				"Customer Name: Auto Canal+ Petit|Order Number: 10211|Amount: 49165.16" + "\n" + 
				"Customer Name: Auto Canal+ Petit|Order Number: 10252|Amount: 49165.16" + "\n" + 
				"Customer Name: Auto Canal+ Petit|Order Number: 10402|Amount: 49165.16" + "\n" + 
				"Customer Name: Auto Canal+ Petit|Order Number: 10211|Amount: 25080.96" + "\n" + 
				"Customer Name: Auto Canal+ Petit|Order Number: 10252|Amount: 25080.96" + "\n" + 
				"Customer Name: Auto Canal+ Petit|Order Number: 10402|Amount: 25080.96" + "\n" + 
				"Customer Name: Extreme Desk Decorations, Ltd|Order Number: 10234|Amount: 35034.57" + "\n" + 
				"Customer Name: Extreme Desk Decorations, Ltd|Order Number: 10268|Amount: 35034.57" + "\n" + 
				"Customer Name: Extreme Desk Decorations, Ltd|Order Number: 10418|Amount: 35034.57" + "\n" + 
				"Customer Name: Extreme Desk Decorations, Ltd|Order Number: 10234|Amount: 31670.37" + "\n" + 
				"Customer Name: Extreme Desk Decorations, Ltd|Order Number: 10268|Amount: 31670.37" + "\n" + 
				"Customer Name: Extreme Desk Decorations, Ltd|Order Number: 10418|Amount: 31670.37" + "\n" + 
				"Customer Name: Bavarian Collectables Imports, Co.|Order Number: 10296|Amount: 31310.09" + "\n" + 
				"Customer Name: Classic Legends Inc.|Order Number: 10115|Amount: 25505.98" + "\n" + 
				"Customer Name: Classic Legends Inc.|Order Number: 10163|Amount: 25505.98" + "\n" + 
				"Customer Name: Classic Legends Inc.|Order Number: 10337|Amount: 25505.98" + "\n" + 
				"Customer Name: Gift Ideas Corp.|Order Number: 10131|Amount: 26304.13" + "\n" + 
				"Customer Name: Gift Ideas Corp.|Order Number: 10146|Amount: 26304.13" + "\n" + 
				"Customer Name: Gift Ideas Corp.|Order Number: 10353|Amount: 26304.13" + "\n" + 
				"Customer Name: Scandinavian Gift Ideas|Order Number: 10167|Amount: 27966.54" + "\n" + 
				"Customer Name: Scandinavian Gift Ideas|Order Number: 10291|Amount: 27966.54" + "\n" + 
				"Customer Name: Scandinavian Gift Ideas|Order Number: 10389|Amount: 27966.54" + "\n" + 
				"Customer Name: Scandinavian Gift Ideas|Order Number: 10167|Amount: 48809.9" + "\n" + 
				"Customer Name: Scandinavian Gift Ideas|Order Number: 10291|Amount: 48809.9" + "\n" + 
				"Customer Name: Scandinavian Gift Ideas|Order Number: 10389|Amount: 48809.9" + "\n" + 
				"Customer Name: The Sharp Gifts Warehouse|Order Number: 10250|Amount: 59551.38" + "\n" + 
				"Customer Name: The Sharp Gifts Warehouse|Order Number: 10257|Amount: 59551.38" + "\n" + 
				"Customer Name: The Sharp Gifts Warehouse|Order Number: 10400|Amount: 59551.38" + "\n" + 
				"Customer Name: The Sharp Gifts Warehouse|Order Number: 10407|Amount: 59551.38" + "\n" + 
				"Customer Name: Mini Auto Werke|Order Number: 10164|Amount: 27121.9" + "\n" + 
				"Customer Name: Mini Auto Werke|Order Number: 10170|Amount: 27121.9" + "\n" + 
				"Customer Name: Mini Auto Werke|Order Number: 10392|Amount: 27121.9" + "\n" + 
				"Customer Name: Super Scale Inc.|Order Number: 10196|Amount: 38139.18" + "\n" + 
				"Customer Name: Super Scale Inc.|Order Number: 10245|Amount: 38139.18" + "\n" + 
				"Customer Name: Super Scale Inc.|Order Number: 10196|Amount: 32239.47" + "\n" + 
				"Customer Name: Super Scale Inc.|Order Number: 10245|Amount: 32239.47" + "\n" + 
				"Customer Name: Microscale Inc.|Order Number: 10242|Amount: 27550.51" + "\n" + 
				"Customer Name: Microscale Inc.|Order Number: 10319|Amount: 27550.51" + "\n" + 
				"Customer Name: Corrida Auto Replicas, Ltd|Order Number: 10126|Amount: 33145.56" + "\n" + 
				"Customer Name: Corrida Auto Replicas, Ltd|Order Number: 10214|Amount: 33145.56" + "\n" + 
				"Customer Name: Corrida Auto Replicas, Ltd|Order Number: 10348|Amount: 33145.56" + "\n" + 
				"Customer Name: Corrida Auto Replicas, Ltd|Order Number: 10126|Amount: 57131.92" + "\n" + 
				"Customer Name: Corrida Auto Replicas, Ltd|Order Number: 10214|Amount: 57131.92" + "\n" + 
				"Customer Name: Corrida Auto Replicas, Ltd|Order Number: 10348|Amount: 57131.92" + "\n" + 
				"Customer Name: FunGiftIdeas.com|Order Number: 10166|Amount: 30293.77" + "\n" + 
				"Customer Name: FunGiftIdeas.com|Order Number: 10321|Amount: 30293.77" + "\n" + 
				"Customer Name: FunGiftIdeas.com|Order Number: 10388|Amount: 30293.77" + "\n" + 
				"Customer Name: FunGiftIdeas.com|Order Number: 10166|Amount: 48355.87" + "\n" + 
				"Customer Name: FunGiftIdeas.com|Order Number: 10321|Amount: 48355.87" + "\n" + 
				"Customer Name: FunGiftIdeas.com|Order Number: 10388|Amount: 48355.87" + "\n" + 
				"Customer Name: Australian Collectables, Ltd|Order Number: 10193|Amount: 35505.63" + "\n" + 
				"Customer Name: Australian Collectables, Ltd|Order Number: 10265|Amount: 35505.63" + "\n" + 
				"Customer Name: Australian Collectables, Ltd|Order Number: 10415|Amount: 35505.63" + "\n" + 
				"Customer Name: West Coast Collectables Co.|Order Number: 10199|Amount: 36070.47" + "\n" + 
				"Customer Name: West Coast Collectables Co.|Order Number: 10215|Amount: 36070.47" + "\n" + 
				"Customer Name: Iberia Gift Imports, Corp.|Order Number: 10184|Amount: 47513.19" + "\n" + 
				"Customer Name: Iberia Gift Imports, Corp.|Order Number: 10303|Amount: 47513.19" + "\n" + 
				"Customer Name: Motor Mint Distributors Inc.|Order Number: 10109|Amount: 45994.07" + "\n" + 
				"Customer Name: Motor Mint Distributors Inc.|Order Number: 10236|Amount: 45994.07" + "\n" + 
				"Customer Name: Motor Mint Distributors Inc.|Order Number: 10331|Amount: 45994.07" + "\n" + 
				"Customer Name: Motor Mint Distributors Inc.|Order Number: 10109|Amount: 25833.14" + "\n" + 
				"Customer Name: Motor Mint Distributors Inc.|Order Number: 10236|Amount: 25833.14" + "\n" + 
				"Customer Name: Motor Mint Distributors Inc.|Order Number: 10331|Amount: 25833.14" + "\n" + 
				"Customer Name: Signal Collectibles Ltd.|Order Number: 10149|Amount: 29997.09" + "\n" + 
				"Customer Name: Signal Collectibles Ltd.|Order Number: 10219|Amount: 29997.09" + "\n" + 
				"Customer Name: Diecast Collectables|Order Number: 10207|Amount: 59265.14" + "\n" + 
				"Customer Name: Diecast Collectables|Order Number: 10243|Amount: 59265.14" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10138|Amount: 30253.75" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10179|Amount: 30253.75" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10360|Amount: 30253.75" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10399|Amount: 30253.75" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10138|Amount: 32077.44" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10179|Amount: 32077.44" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10360|Amount: 32077.44" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10399|Amount: 32077.44" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10138|Amount: 52166.0" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10179|Amount: 52166.0" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10360|Amount: 52166.0" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10399|Amount: 52166.0" + "\n",
				test25k.Over25000());

	}

	@Test //Must fix java.lang.OutofMemoryError otherwise test will fail 
	public void testOver25kQuery() {
		Requirement3 test25kQuery = new Requirement3("root", "");

		test25kQuery.getCustomerQuery();
		test25kQuery.getOrdersQuery();
		test25kQuery.getPaymentQuery();

		assertEquals("Customers with orders above $25,000:" + "\n" + 
				"Customer Name: Signal Gift Stores|Order Number: 10124|Amount: 32641.98" + "\n" + 
				"Customer Name: Signal Gift Stores|Order Number: 10278|Amount: 32641.98" + "\n" + 
				"Customer Name: Signal Gift Stores|Order Number: 10346|Amount: 32641.98" + "\n" + 
				"Customer Name: Signal Gift Stores|Order Number: 10124|Amount: 33347.88" + "\n" + 
				"Customer Name: Signal Gift Stores|Order Number: 10278|Amount: 33347.88" + "\n" + 
				"Customer Name: Signal Gift Stores|Order Number: 10346|Amount: 33347.88" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10120|Amount: 45864.03" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10125|Amount: 45864.03" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10223|Amount: 45864.03" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10342|Amount: 45864.03" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10347|Amount: 45864.03" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10120|Amount: 82261.22" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10125|Amount: 82261.22" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10223|Amount: 82261.22" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10342|Amount: 82261.22" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10347|Amount: 82261.22" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10120|Amount: 44894.74" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10125|Amount: 44894.74" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10223|Amount: 44894.74" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10342|Amount: 44894.74" + "\n" + 
				"Customer Name: Australian Collectors, Co.|Order Number: 10347|Amount: 44894.74" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10275|Amount: 47924.19" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10315|Amount: 47924.19" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10375|Amount: 47924.19" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10425|Amount: 47924.19" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10275|Amount: 49523.67" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10315|Amount: 49523.67" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10375|Amount: 49523.67" + "\n" + 
				"Customer Name: La Rochelle Gifts|Order Number: 10425|Amount: 49523.67" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10103|Amount: 50218.95" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10158|Amount: 50218.95" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10309|Amount: 50218.95" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10325|Amount: 50218.95" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10103|Amount: 34638.14" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10158|Amount: 34638.14" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10309|Amount: 34638.14" + "\n" + 
				"Customer Name: Baane Mini Imports|Order Number: 10325|Amount: 34638.14" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 101244.59" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 85410.87" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 83598.04" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 47142.7" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 55639.66" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 111654.4" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 43369.3" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10113|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10135|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10142|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10182|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10229|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10271|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10282|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10312|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10335|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10357|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10368|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10371|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10382|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10385|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10390|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10396|Amount: 45084.38" + "\n" + 
				"Customer Name: Mini Gifts Distributors Ltd.|Order Number: 10421|Amount: 45084.38" + "\n" + 
				"Customer Name: Blauer See Auto, Co.|Order Number: 10101|Amount: 33820.62" + "\n" + 
				"Customer Name: Blauer See Auto, Co.|Order Number: 10230|Amount: 33820.62" + "\n" + 
				"Customer Name: Blauer See Auto, Co.|Order Number: 10300|Amount: 33820.62" + "\n" + 
				"Customer Name: Blauer See Auto, Co.|Order Number: 10323|Amount: 33820.62" + "\n" + 
				"Customer Name: Mini Wheels Co.|Order Number: 10111|Amount: 26248.78" + "\n" + 
				"Customer Name: Mini Wheels Co.|Order Number: 10201|Amount: 26248.78" + "\n" + 
				"Customer Name: Mini Wheels Co.|Order Number: 10333|Amount: 26248.78" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10107|Amount: 50025.35" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10248|Amount: 50025.35" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10292|Amount: 50025.35" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10329|Amount: 50025.35" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10107|Amount: 35321.97" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10248|Amount: 35321.97" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10292|Amount: 35321.97" + "\n" + 
				"Customer Name: Land of Toys Inc.|Order Number: 10329|Amount: 35321.97" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 36251.03" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 36140.38" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 46895.48" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 59830.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 116208.4" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 65071.26" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 120166.58" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 49539.37" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 40206.2" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 63843.55" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 35420.74" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10104|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10128|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10133|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10153|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10156|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10190|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10203|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10205|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10212|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10244|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10246|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10262|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10279|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10311|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10350|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10355|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10358|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10378|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10379|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10380|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10383|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10386|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10394|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10412|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10417|Amount: 26155.91" + "\n" + 
				"Customer Name: Euro+ Shopping Channel|Order Number: 10424|Amount: 26155.91" + "\n" + 
				"Customer Name: Volvo Model Replicas, Co|Order Number: 10112|Amount: 36005.71" + "\n" + 
				"Customer Name: Volvo Model Replicas, Co|Order Number: 10320|Amount: 36005.71" + "\n" + 
				"Customer Name: Volvo Model Replicas, Co|Order Number: 10326|Amount: 36005.71" + "\n" + 
				"Customer Name: Volvo Model Replicas, Co|Order Number: 10334|Amount: 36005.71" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10105|Amount: 28211.7" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10238|Amount: 28211.7" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10256|Amount: 28211.7" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10327|Amount: 28211.7" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10406|Amount: 28211.7" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10105|Amount: 53959.21" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10238|Amount: 53959.21" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10256|Amount: 53959.21" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10327|Amount: 53959.21" + "\n" + 
				"Customer Name: Danish Wholesale Imports|Order Number: 10406|Amount: 53959.21" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10194|Amount: 40978.53" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10208|Amount: 40978.53" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10227|Amount: 40978.53" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10194|Amount: 49614.72" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10208|Amount: 49614.72" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10227|Amount: 49614.72" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10194|Amount: 39712.1" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10208|Amount: 39712.1" + "\n" + 
				"Customer Name: Saveley & Henriot, Co.|Order Number: 10227|Amount: 39712.1" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10117|Amount: 44380.15" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10150|Amount: 44380.15" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10165|Amount: 44380.15" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10277|Amount: 44380.15" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10387|Amount: 44380.15" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10117|Amount: 105743.0" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10150|Amount: 105743.0" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10165|Amount: 105743.0" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10277|Amount: 105743.0" + "\n" + 
				"Customer Name: Dragon Souveniers, Ltd.|Order Number: 10387|Amount: 105743.0" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10127|Amount: 58793.53" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10204|Amount: 58793.53" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10267|Amount: 58793.53" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10349|Amount: 58793.53" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10127|Amount: 58841.35" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10204|Amount: 58841.35" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10267|Amount: 58841.35" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10349|Amount: 58841.35" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10127|Amount: 39964.63" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10204|Amount: 39964.63" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10267|Amount: 39964.63" + "\n" + 
				"Customer Name: Muscle Machine Inc|Order Number: 10349|Amount: 39964.63" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10272|Amount: 35152.12" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10281|Amount: 35152.12" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10318|Amount: 35152.12" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10422|Amount: 35152.12" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10272|Amount: 63357.13" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10281|Amount: 63357.13" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10318|Amount: 63357.13" + "\n" + 
				"Customer Name: Diecast Classics Inc.|Order Number: 10422|Amount: 63357.13" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10140|Amount: 50743.65" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10168|Amount: 50743.65" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10317|Amount: 50743.65" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10362|Amount: 50743.65" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10140|Amount: 38675.13" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10168|Amount: 38675.13" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10317|Amount: 38675.13" + "\n" + 
				"Customer Name: Technics Stores Inc.|Order Number: 10362|Amount: 38675.13" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10217|Amount: 38785.48" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10259|Amount: 38785.48" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10288|Amount: 38785.48" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10409|Amount: 38785.48" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10217|Amount: 44160.92" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10259|Amount: 44160.92" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10288|Amount: 44160.92" + "\n" + 
				"Customer Name: Handji Gifts& Co|Order Number: 10409|Amount: 44160.92" + "\n" + 
				"Customer Name: Herkku Gifts|Order Number: 10181|Amount: 85024.46" + "\n" + 
				"Customer Name: Herkku Gifts|Order Number: 10188|Amount: 85024.46" + "\n" + 
				"Customer Name: Herkku Gifts|Order Number: 10289|Amount: 85024.46" + "\n" + 
				"Customer Name: Daedalus Designs Imports|Order Number: 10180|Amount: 42783.81" + "\n" + 
				"Customer Name: Daedalus Designs Imports|Order Number: 10224|Amount: 42783.81" + "\n" + 
				"Customer Name: La Corne D'abondance, Co.|Order Number: 10114|Amount: 51209.58" + "\n" + 
				"Customer Name: La Corne D'abondance, Co.|Order Number: 10286|Amount: 51209.58" + "\n" + 
				"Customer Name: La Corne D'abondance, Co.|Order Number: 10336|Amount: 51209.58" + "\n" + 
				"Customer Name: La Corne D'abondance, Co.|Order Number: 10114|Amount: 33383.14" + "\n" + 
				"Customer Name: La Corne D'abondance, Co.|Order Number: 10286|Amount: 33383.14" + "\n" + 
				"Customer Name: La Corne D'abondance, Co.|Order Number: 10336|Amount: 33383.14" + "\n" + 
				"Customer Name: Gift Depot Inc.|Order Number: 10172|Amount: 28500.78" + "\n" + 
				"Customer Name: Gift Depot Inc.|Order Number: 10263|Amount: 28500.78" + "\n" + 
				"Customer Name: Gift Depot Inc.|Order Number: 10413|Amount: 28500.78" + "\n" + 
				"Customer Name: Gift Depot Inc.|Order Number: 10172|Amount: 42044.77" + "\n" + 
				"Customer Name: Gift Depot Inc.|Order Number: 10263|Amount: 42044.77" + "\n" + 
				"Customer Name: Gift Depot Inc.|Order Number: 10413|Amount: 42044.77" + "\n" + 
				"Customer Name: Osaka Souveniers Co.|Order Number: 10210|Amount: 47177.59" + "\n" + 
				"Customer Name: Osaka Souveniers Co.|Order Number: 10240|Amount: 47177.59" + "\n" + 
				"Customer Name: Vitachrome Inc.|Order Number: 10102|Amount: 44400.5" + "\n" + 
				"Customer Name: Vitachrome Inc.|Order Number: 10237|Amount: 44400.5" + "\n" + 
				"Customer Name: Vitachrome Inc.|Order Number: 10324|Amount: 44400.5" + "\n" + 
				"Customer Name: Toys of Finland, Co.|Order Number: 10155|Amount: 37602.48" + "\n" + 
				"Customer Name: Toys of Finland, Co.|Order Number: 10299|Amount: 37602.48" + "\n" + 
				"Customer Name: Toys of Finland, Co.|Order Number: 10377|Amount: 37602.48" + "\n" + 
				"Customer Name: Toys of Finland, Co.|Order Number: 10155|Amount: 34341.08" + "\n" + 
				"Customer Name: Toys of Finland, Co.|Order Number: 10299|Amount: 34341.08" + "\n" + 
				"Customer Name: Toys of Finland, Co.|Order Number: 10377|Amount: 34341.08" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10110|Amount: 52825.29" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10306|Amount: 52825.29" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10332|Amount: 52825.29" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10110|Amount: 47159.11" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10306|Amount: 47159.11" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10332|Amount: 47159.11" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10110|Amount: 48425.69" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10306|Amount: 48425.69" + "\n" + 
				"Customer Name: AV Stores, Co.|Order Number: 10332|Amount: 48425.69" + "\n" + 
				"Customer Name: Clover Collections, Co.|Order Number: 10220|Amount: 32538.74" + "\n" + 
				"Customer Name: Clover Collections, Co.|Order Number: 10297|Amount: 32538.74" + "\n" + 
				"Customer Name: UK Collectables, Ltd.|Order Number: 10253|Amount: 37258.94" + "\n" + 
				"Customer Name: UK Collectables, Ltd.|Order Number: 10302|Amount: 37258.94" + "\n" + 
				"Customer Name: UK Collectables, Ltd.|Order Number: 10403|Amount: 37258.94" + "\n" + 
				"Customer Name: Canadian Gift Exchange Network|Order Number: 10206|Amount: 36527.61" + "\n" + 
				"Customer Name: Canadian Gift Exchange Network|Order Number: 10313|Amount: 36527.61" + "\n" + 
				"Customer Name: Canadian Gift Exchange Network|Order Number: 10206|Amount: 33594.58" + "\n" + 
				"Customer Name: Canadian Gift Exchange Network|Order Number: 10313|Amount: 33594.58" + "\n" + 
				"Customer Name: Online Mini Collectables|Order Number: 10276|Amount: 51152.86" + "\n" + 
				"Customer Name: Online Mini Collectables|Order Number: 10294|Amount: 51152.86" + "\n" + 
				"Customer Name: Toys4GrownUps.com|Order Number: 10145|Amount: 50342.74" + "\n" + 
				"Customer Name: Toys4GrownUps.com|Order Number: 10189|Amount: 50342.74" + "\n" + 
				"Customer Name: Toys4GrownUps.com|Order Number: 10367|Amount: 50342.74" + "\n" + 
				"Customer Name: Toys4GrownUps.com|Order Number: 10145|Amount: 39580.6" + "\n" + 
				"Customer Name: Toys4GrownUps.com|Order Number: 10189|Amount: 39580.6" + "\n" + 
				"Customer Name: Toys4GrownUps.com|Order Number: 10367|Amount: 39580.6" + "\n" + 
				"Customer Name: Mini Caravy|Order Number: 10241|Amount: 35157.75" + "\n" + 
				"Customer Name: Mini Caravy|Order Number: 10255|Amount: 35157.75" + "\n" + 
				"Customer Name: Mini Caravy|Order Number: 10405|Amount: 35157.75" + "\n" + 
				"Customer Name: Mini Caravy|Order Number: 10241|Amount: 36069.26" + "\n" + 
				"Customer Name: Mini Caravy|Order Number: 10255|Amount: 36069.26" + "\n" + 
				"Customer Name: Mini Caravy|Order Number: 10405|Amount: 36069.26" + "\n" + 
				"Customer Name: King Kong Collectables, Co.|Order Number: 10187|Amount: 45480.79" + "\n" + 
				"Customer Name: King Kong Collectables, Co.|Order Number: 10200|Amount: 45480.79" + "\n" + 
				"Customer Name: Enaco Distributors|Order Number: 10118|Amount: 40473.86" + "\n" + 
				"Customer Name: Enaco Distributors|Order Number: 10197|Amount: 40473.86" + "\n" + 
				"Customer Name: Enaco Distributors|Order Number: 10340|Amount: 40473.86" + "\n" + 
				"Customer Name: Heintze Collectables|Order Number: 10161|Amount: 36164.46" + "\n" + 
				"Customer Name: Heintze Collectables|Order Number: 10314|Amount: 36164.46" + "\n" + 
				"Customer Name: Heintze Collectables|Order Number: 10161|Amount: 53745.34" + "\n" + 
				"Customer Name: Heintze Collectables|Order Number: 10314|Amount: 53745.34" + "\n" + 
				"Customer Name: Qu�bec Home Shopping Network|Order Number: 10171|Amount: 29070.38" + "\n" + 
				"Customer Name: Qu�bec Home Shopping Network|Order Number: 10261|Amount: 29070.38" + "\n" + 
				"Customer Name: Qu�bec Home Shopping Network|Order Number: 10411|Amount: 29070.38" + "\n" + 
				"Customer Name: Collectable Mini Designs Co.|Order Number: 10222|Amount: 80375.24" + "\n" + 
				"Customer Name: Collectable Mini Designs Co.|Order Number: 10226|Amount: 80375.24" + "\n" + 
				"Customer Name: giftsbymail.co.uk|Order Number: 10232|Amount: 46788.14" + "\n" + 
				"Customer Name: giftsbymail.co.uk|Order Number: 10316|Amount: 46788.14" + "\n" + 
				"Customer Name: Alpha Cognac|Order Number: 10136|Amount: 33818.34" + "\n" + 
				"Customer Name: Alpha Cognac|Order Number: 10178|Amount: 33818.34" + "\n" + 
				"Customer Name: Alpha Cognac|Order Number: 10397|Amount: 33818.34" + "\n" + 
				"Customer Name: Amica Models & Co.|Order Number: 10280|Amount: 33924.24" + "\n" + 
				"Customer Name: Amica Models & Co.|Order Number: 10293|Amount: 33924.24" + "\n" + 
				"Customer Name: Amica Models & Co.|Order Number: 10280|Amount: 48298.99" + "\n" + 
				"Customer Name: Amica Models & Co.|Order Number: 10293|Amount: 48298.99" + "\n" + 
				"Customer Name: Lyon Souveniers|Order Number: 10134|Amount: 26311.63" + "\n" + 
				"Customer Name: Lyon Souveniers|Order Number: 10356|Amount: 26311.63" + "\n" + 
				"Customer Name: Lyon Souveniers|Order Number: 10395|Amount: 26311.63" + "\n" + 
				"Customer Name: Auto Associ�s & Cie.|Order Number: 10216|Amount: 53116.99" + "\n" + 
				"Customer Name: Auto Associ�s & Cie.|Order Number: 10304|Amount: 53116.99" + "\n" + 
				"Customer Name: Toms Spezialit�ten, Ltd|Order Number: 10191|Amount: 61234.67" + "\n" + 
				"Customer Name: Toms Spezialit�ten, Ltd|Order Number: 10310|Amount: 61234.67" + "\n" + 
				"Customer Name: Toms Spezialit�ten, Ltd|Order Number: 10191|Amount: 27988.47" + "\n" + 
				"Customer Name: Toms Spezialit�ten, Ltd|Order Number: 10310|Amount: 27988.47" + "\n" + 
				"Customer Name: Royal Canadian Collectables, Ltd.|Order Number: 10235|Amount: 37527.58" + "\n" + 
				"Customer Name: Royal Canadian Collectables, Ltd.|Order Number: 10283|Amount: 37527.58" + "\n" + 
				"Customer Name: Royal Canadian Collectables, Ltd.|Order Number: 10235|Amount: 29284.42" + "\n" + 
				"Customer Name: Royal Canadian Collectables, Ltd.|Order Number: 10283|Amount: 29284.42" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10148|Amount: 27083.78" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10169|Amount: 27083.78" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10370|Amount: 27083.78" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10391|Amount: 27083.78" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10148|Amount: 38547.19" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10169|Amount: 38547.19" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10370|Amount: 38547.19" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10391|Amount: 38547.19" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10148|Amount: 41554.73" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10169|Amount: 41554.73" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10370|Amount: 41554.73" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10391|Amount: 41554.73" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10148|Amount: 29848.52" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10169|Amount: 29848.52" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10370|Amount: 29848.52" + "\n" + 
				"Customer Name: Anna's Decorations, Ltd|Order Number: 10391|Amount: 29848.52" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10106|Amount: 37654.09" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10173|Amount: 37654.09" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10328|Amount: 37654.09" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10106|Amount: 52151.81" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10173|Amount: 52151.81" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10328|Amount: 52151.81" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10106|Amount: 37723.79" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10173|Amount: 37723.79" + "\n" + 
				"Customer Name: Rovelli Gifts|Order Number: 10328|Amount: 37723.79" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10139|Amount: 35806.73" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10270|Amount: 35806.73" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10361|Amount: 35806.73" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10420|Amount: 35806.73" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10139|Amount: 31835.36" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10270|Amount: 31835.36" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10361|Amount: 31835.36" + "\n" + 
				"Customer Name: Souveniers And Things Co.|Order Number: 10420|Amount: 31835.36" + "\n" + 
				"Customer Name: Marta's Replicas Co.|Order Number: 10285|Amount: 47411.33" + "\n" + 
				"Customer Name: Marta's Replicas Co.|Order Number: 10305|Amount: 47411.33" + "\n" + 
				"Customer Name: Marta's Replicas Co.|Order Number: 10285|Amount: 43134.04" + "\n" + 
				"Customer Name: Marta's Replicas Co.|Order Number: 10305|Amount: 43134.04" + "\n" + 
				"Customer Name: Vida Sport, Ltd|Order Number: 10225|Amount: 47375.92" + "\n" + 
				"Customer Name: Vida Sport, Ltd|Order Number: 10287|Amount: 47375.92" + "\n" + 
				"Customer Name: Vida Sport, Ltd|Order Number: 10225|Amount: 61402.0" + "\n" + 
				"Customer Name: Vida Sport, Ltd|Order Number: 10287|Amount: 61402.0" + "\n" + 
				"Customer Name: Norway Gifts By Mail, Co.|Order Number: 10284|Amount: 36798.88" + "\n" + 
				"Customer Name: Norway Gifts By Mail, Co.|Order Number: 10301|Amount: 36798.88" + "\n" + 
				"Customer Name: Norway Gifts By Mail, Co.|Order Number: 10284|Amount: 32260.16" + "\n" + 
				"Customer Name: Norway Gifts By Mail, Co.|Order Number: 10301|Amount: 32260.16" + "\n" + 
				"Customer Name: Oulu Toy Supplies, Inc.|Order Number: 10151|Amount: 46770.52" + "\n" + 
				"Customer Name: Oulu Toy Supplies, Inc.|Order Number: 10239|Amount: 46770.52" + "\n" + 
				"Customer Name: Oulu Toy Supplies, Inc.|Order Number: 10373|Amount: 46770.52" + "\n" + 
				"Customer Name: Oulu Toy Supplies, Inc.|Order Number: 10151|Amount: 32723.04" + "\n" + 
				"Customer Name: Oulu Toy Supplies, Inc.|Order Number: 10239|Amount: 32723.04" + "\n" + 
				"Customer Name: Oulu Toy Supplies, Inc.|Order Number: 10373|Amount: 32723.04" + "\n" + 
				"Customer Name: Petit Auto|Order Number: 10221|Amount: 45352.47" + "\n" + 
				"Customer Name: Petit Auto|Order Number: 10273|Amount: 45352.47" + "\n" + 
				"Customer Name: Petit Auto|Order Number: 10423|Amount: 45352.47" + "\n" + 
				"Customer Name: Mini Classics|Order Number: 10195|Amount: 42339.76" + "\n" + 
				"Customer Name: Mini Classics|Order Number: 10308|Amount: 42339.76" + "\n" + 
				"Customer Name: Mini Classics|Order Number: 10195|Amount: 36092.4" + "\n" + 
				"Customer Name: Mini Classics|Order Number: 10308|Amount: 36092.4" + "\n" + 
				"Customer Name: Mini Creations Ltd.|Order Number: 10143|Amount: 41016.75" + "\n" + 
				"Customer Name: Mini Creations Ltd.|Order Number: 10185|Amount: 41016.75" + "\n" + 
				"Customer Name: Mini Creations Ltd.|Order Number: 10365|Amount: 41016.75" + "\n" + 
				"Customer Name: Mini Creations Ltd.|Order Number: 10143|Amount: 52548.49" + "\n" + 
				"Customer Name: Mini Creations Ltd.|Order Number: 10185|Amount: 52548.49" + "\n" + 
				"Customer Name: Mini Creations Ltd.|Order Number: 10365|Amount: 52548.49" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10159|Amount: 85559.12" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10162|Amount: 85559.12" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10381|Amount: 85559.12" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10384|Amount: 85559.12" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10159|Amount: 46781.66" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10162|Amount: 46781.66" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10381|Amount: 46781.66" + "\n" + 
				"Customer Name: Corporate Gift Ideas Co.|Order Number: 10384|Amount: 46781.66" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10132|Amount: 75020.13" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10254|Amount: 75020.13" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10354|Amount: 75020.13" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10393|Amount: 75020.13" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10404|Amount: 75020.13" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10132|Amount: 37281.36" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10254|Amount: 37281.36" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10354|Amount: 37281.36" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10393|Amount: 37281.36" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10404|Amount: 37281.36" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10132|Amount: 39440.59" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10254|Amount: 39440.59" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10354|Amount: 39440.59" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10393|Amount: 39440.59" + "\n" + 
				"Customer Name: Down Under Souveniers, Inc|Order Number: 10404|Amount: 39440.59" + "\n" + 
				"Customer Name: Stylish Desk Decors, Co.|Order Number: 10129|Amount: 29429.14" + "\n" + 
				"Customer Name: Stylish Desk Decors, Co.|Order Number: 10175|Amount: 29429.14" + "\n" + 
				"Customer Name: Stylish Desk Decors, Co.|Order Number: 10351|Amount: 29429.14" + "\n" + 
				"Customer Name: Stylish Desk Decors, Co.|Order Number: 10129|Amount: 37455.77" + "\n" + 
				"Customer Name: Stylish Desk Decors, Co.|Order Number: 10175|Amount: 37455.77" + "\n" + 
				"Customer Name: Stylish Desk Decors, Co.|Order Number: 10351|Amount: 37455.77" + "\n" + 
				"Customer Name: Tekni Collectables Inc.|Order Number: 10233|Amount: 31102.85" + "\n" + 
				"Customer Name: Tekni Collectables Inc.|Order Number: 10251|Amount: 31102.85" + "\n" + 
				"Customer Name: Tekni Collectables Inc.|Order Number: 10401|Amount: 31102.85" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10141|Amount: 45785.34" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10247|Amount: 45785.34" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10363|Amount: 45785.34" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10141|Amount: 29716.86" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10247|Amount: 29716.86" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10363|Amount: 29716.86" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10141|Amount: 28394.54" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10247|Amount: 28394.54" + "\n" + 
				"Customer Name: Suominen Souveniers|Order Number: 10363|Amount: 28394.54" + "\n" + 
				"Customer Name: Classic Gift Ideas, Inc|Order Number: 10183|Amount: 34606.28" + "\n" + 
				"Customer Name: Classic Gift Ideas, Inc|Order Number: 10307|Amount: 34606.28" + "\n" + 
				"Customer Name: CAF Imports|Order Number: 10177|Amount: 31428.21" + "\n" + 
				"Customer Name: CAF Imports|Order Number: 10231|Amount: 31428.21" + "\n" + 
				"Customer Name: Marseille Mini Autos|Order Number: 10122|Amount: 50824.66" + "\n" + 
				"Customer Name: Marseille Mini Autos|Order Number: 10344|Amount: 50824.66" + "\n" + 
				"Customer Name: Marseille Mini Autos|Order Number: 10364|Amount: 50824.66" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10121|Amount: 49705.52" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10137|Amount: 49705.52" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10343|Amount: 49705.52" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10359|Amount: 49705.52" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10398|Amount: 49705.52" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10121|Amount: 46656.94" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10137|Amount: 46656.94" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10343|Amount: 46656.94" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10359|Amount: 46656.94" + "\n" + 
				"Customer Name: Reims Collectables|Order Number: 10398|Amount: 46656.94" + "\n" + 
				"Customer Name: GiftsForHim.com|Order Number: 10202|Amount: 36442.34" + "\n" + 
				"Customer Name: GiftsForHim.com|Order Number: 10260|Amount: 36442.34" + "\n" + 
				"Customer Name: GiftsForHim.com|Order Number: 10410|Amount: 36442.34" + "\n" + 
				"Customer Name: Online Diecast Creations Co.|Order Number: 10100|Amount: 50799.69" + "\n" + 
				"Customer Name: Online Diecast Creations Co.|Order Number: 10192|Amount: 50799.69" + "\n" + 
				"Customer Name: Online Diecast Creations Co.|Order Number: 10322|Amount: 50799.69" + "\n" + 
				"Customer Name: Online Diecast Creations Co.|Order Number: 10100|Amount: 55425.77" + "\n" + 
				"Customer Name: Online Diecast Creations Co.|Order Number: 10192|Amount: 55425.77" + "\n" + 
				"Customer Name: Online Diecast Creations Co.|Order Number: 10322|Amount: 55425.77" + "\n" + 
				"Customer Name: Collectables For Less Inc.|Order Number: 10147|Amount: 28322.83" + "\n" + 
				"Customer Name: Collectables For Less Inc.|Order Number: 10274|Amount: 28322.83" + "\n" + 
				"Customer Name: Collectables For Less Inc.|Order Number: 10369|Amount: 28322.83" + "\n" + 
				"Customer Name: Collectables For Less Inc.|Order Number: 10147|Amount: 32680.31" + "\n" + 
				"Customer Name: Collectables For Less Inc.|Order Number: 10274|Amount: 32680.31" + "\n" + 
				"Customer Name: Collectables For Less Inc.|Order Number: 10369|Amount: 32680.31" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10119|Amount: 35826.33" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10269|Amount: 35826.33" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10341|Amount: 35826.33" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10419|Amount: 35826.33" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10119|Amount: 42813.83" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10269|Amount: 42813.83" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10341|Amount: 42813.83" + "\n" + 
				"Customer Name: Salzburg Collectables|Order Number: 10419|Amount: 42813.83" + "\n" + 
				"Customer Name: Cruz & Sons Co.|Order Number: 10108|Amount: 51001.22" + "\n" + 
				"Customer Name: Cruz & Sons Co.|Order Number: 10198|Amount: 51001.22" + "\n" + 
				"Customer Name: Cruz & Sons Co.|Order Number: 10330|Amount: 51001.22" + "\n" + 
				"Customer Name: L'ordine Souveniers|Order Number: 10176|Amount: 38524.29" + "\n" + 
				"Customer Name: L'ordine Souveniers|Order Number: 10266|Amount: 38524.29" + "\n" + 
				"Customer Name: L'ordine Souveniers|Order Number: 10416|Amount: 38524.29" + "\n" + 
				"Customer Name: L'ordine Souveniers|Order Number: 10176|Amount: 51619.02" + "\n" + 
				"Customer Name: L'ordine Souveniers|Order Number: 10266|Amount: 51619.02" + "\n" + 
				"Customer Name: L'ordine Souveniers|Order Number: 10416|Amount: 51619.02" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10258|Amount: 33967.73" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10339|Amount: 33967.73" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10372|Amount: 33967.73" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10408|Amount: 33967.73" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10258|Amount: 48927.64" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10339|Amount: 48927.64" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10372|Amount: 48927.64" + "\n" + 
				"Customer Name: Tokyo Collectables, Ltd|Order Number: 10408|Amount: 48927.64" + "\n" + 
				"Customer Name: Auto Canal+ Petit|Order Number: 10211|Amount: 49165.16" + "\n" + 
				"Customer Name: Auto Canal+ Petit|Order Number: 10252|Amount: 49165.16" + "\n" + 
				"Customer Name: Auto Canal+ Petit|Order Number: 10402|Amount: 49165.16" + "\n" + 
				"Customer Name: Auto Canal+ Petit|Order Number: 10211|Amount: 25080.96" + "\n" + 
				"Customer Name: Auto Canal+ Petit|Order Number: 10252|Amount: 25080.96" + "\n" + 
				"Customer Name: Auto Canal+ Petit|Order Number: 10402|Amount: 25080.96" + "\n" + 
				"Customer Name: Extreme Desk Decorations, Ltd|Order Number: 10234|Amount: 35034.57" + "\n" + 
				"Customer Name: Extreme Desk Decorations, Ltd|Order Number: 10268|Amount: 35034.57" + "\n" + 
				"Customer Name: Extreme Desk Decorations, Ltd|Order Number: 10418|Amount: 35034.57" + "\n" + 
				"Customer Name: Extreme Desk Decorations, Ltd|Order Number: 10234|Amount: 31670.37" + "\n" + 
				"Customer Name: Extreme Desk Decorations, Ltd|Order Number: 10268|Amount: 31670.37" + "\n" + 
				"Customer Name: Extreme Desk Decorations, Ltd|Order Number: 10418|Amount: 31670.37" + "\n" + 
				"Customer Name: Bavarian Collectables Imports, Co.|Order Number: 10296|Amount: 31310.09" + "\n" + 
				"Customer Name: Classic Legends Inc.|Order Number: 10115|Amount: 25505.98" + "\n" + 
				"Customer Name: Classic Legends Inc.|Order Number: 10163|Amount: 25505.98" + "\n" + 
				"Customer Name: Classic Legends Inc.|Order Number: 10337|Amount: 25505.98" + "\n" + 
				"Customer Name: Gift Ideas Corp.|Order Number: 10131|Amount: 26304.13" + "\n" + 
				"Customer Name: Gift Ideas Corp.|Order Number: 10146|Amount: 26304.13" + "\n" + 
				"Customer Name: Gift Ideas Corp.|Order Number: 10353|Amount: 26304.13" + "\n" + 
				"Customer Name: Scandinavian Gift Ideas|Order Number: 10167|Amount: 27966.54" + "\n" + 
				"Customer Name: Scandinavian Gift Ideas|Order Number: 10291|Amount: 27966.54" + "\n" + 
				"Customer Name: Scandinavian Gift Ideas|Order Number: 10389|Amount: 27966.54" + "\n" + 
				"Customer Name: Scandinavian Gift Ideas|Order Number: 10167|Amount: 48809.9" + "\n" + 
				"Customer Name: Scandinavian Gift Ideas|Order Number: 10291|Amount: 48809.9" + "\n" + 
				"Customer Name: Scandinavian Gift Ideas|Order Number: 10389|Amount: 48809.9" + "\n" + 
				"Customer Name: The Sharp Gifts Warehouse|Order Number: 10250|Amount: 59551.38" + "\n" + 
				"Customer Name: The Sharp Gifts Warehouse|Order Number: 10257|Amount: 59551.38" + "\n" + 
				"Customer Name: The Sharp Gifts Warehouse|Order Number: 10400|Amount: 59551.38" + "\n" + 
				"Customer Name: The Sharp Gifts Warehouse|Order Number: 10407|Amount: 59551.38" + "\n" + 
				"Customer Name: Mini Auto Werke|Order Number: 10164|Amount: 27121.9" + "\n" + 
				"Customer Name: Mini Auto Werke|Order Number: 10170|Amount: 27121.9" + "\n" + 
				"Customer Name: Mini Auto Werke|Order Number: 10392|Amount: 27121.9" + "\n" + 
				"Customer Name: Super Scale Inc.|Order Number: 10196|Amount: 38139.18" + "\n" + 
				"Customer Name: Super Scale Inc.|Order Number: 10245|Amount: 38139.18" + "\n" + 
				"Customer Name: Super Scale Inc.|Order Number: 10196|Amount: 32239.47" + "\n" + 
				"Customer Name: Super Scale Inc.|Order Number: 10245|Amount: 32239.47" + "\n" + 
				"Customer Name: Microscale Inc.|Order Number: 10242|Amount: 27550.51" + "\n" + 
				"Customer Name: Microscale Inc.|Order Number: 10319|Amount: 27550.51" + "\n" + 
				"Customer Name: Corrida Auto Replicas, Ltd|Order Number: 10126|Amount: 33145.56" + "\n" + 
				"Customer Name: Corrida Auto Replicas, Ltd|Order Number: 10214|Amount: 33145.56" + "\n" + 
				"Customer Name: Corrida Auto Replicas, Ltd|Order Number: 10348|Amount: 33145.56" + "\n" + 
				"Customer Name: Corrida Auto Replicas, Ltd|Order Number: 10126|Amount: 57131.92" + "\n" + 
				"Customer Name: Corrida Auto Replicas, Ltd|Order Number: 10214|Amount: 57131.92" + "\n" + 
				"Customer Name: Corrida Auto Replicas, Ltd|Order Number: 10348|Amount: 57131.92" + "\n" + 
				"Customer Name: FunGiftIdeas.com|Order Number: 10166|Amount: 30293.77" + "\n" + 
				"Customer Name: FunGiftIdeas.com|Order Number: 10321|Amount: 30293.77" + "\n" + 
				"Customer Name: FunGiftIdeas.com|Order Number: 10388|Amount: 30293.77" + "\n" + 
				"Customer Name: FunGiftIdeas.com|Order Number: 10166|Amount: 48355.87" + "\n" + 
				"Customer Name: FunGiftIdeas.com|Order Number: 10321|Amount: 48355.87" + "\n" + 
				"Customer Name: FunGiftIdeas.com|Order Number: 10388|Amount: 48355.87" + "\n" + 
				"Customer Name: Australian Collectables, Ltd|Order Number: 10193|Amount: 35505.63" + "\n" + 
				"Customer Name: Australian Collectables, Ltd|Order Number: 10265|Amount: 35505.63" + "\n" + 
				"Customer Name: Australian Collectables, Ltd|Order Number: 10415|Amount: 35505.63" + "\n" + 
				"Customer Name: West Coast Collectables Co.|Order Number: 10199|Amount: 36070.47" + "\n" + 
				"Customer Name: West Coast Collectables Co.|Order Number: 10215|Amount: 36070.47" + "\n" + 
				"Customer Name: Iberia Gift Imports, Corp.|Order Number: 10184|Amount: 47513.19" + "\n" + 
				"Customer Name: Iberia Gift Imports, Corp.|Order Number: 10303|Amount: 47513.19" + "\n" + 
				"Customer Name: Motor Mint Distributors Inc.|Order Number: 10109|Amount: 45994.07" + "\n" + 
				"Customer Name: Motor Mint Distributors Inc.|Order Number: 10236|Amount: 45994.07" + "\n" + 
				"Customer Name: Motor Mint Distributors Inc.|Order Number: 10331|Amount: 45994.07" + "\n" + 
				"Customer Name: Motor Mint Distributors Inc.|Order Number: 10109|Amount: 25833.14" + "\n" + 
				"Customer Name: Motor Mint Distributors Inc.|Order Number: 10236|Amount: 25833.14" + "\n" + 
				"Customer Name: Motor Mint Distributors Inc.|Order Number: 10331|Amount: 25833.14" + "\n" + 
				"Customer Name: Signal Collectibles Ltd.|Order Number: 10149|Amount: 29997.09" + "\n" + 
				"Customer Name: Signal Collectibles Ltd.|Order Number: 10219|Amount: 29997.09" + "\n" + 
				"Customer Name: Diecast Collectables|Order Number: 10207|Amount: 59265.14" + "\n" + 
				"Customer Name: Diecast Collectables|Order Number: 10243|Amount: 59265.14" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10138|Amount: 30253.75" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10179|Amount: 30253.75" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10360|Amount: 30253.75" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10399|Amount: 30253.75" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10138|Amount: 32077.44" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10179|Amount: 32077.44" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10360|Amount: 32077.44" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10399|Amount: 32077.44" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10138|Amount: 52166.0" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10179|Amount: 52166.0" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10360|Amount: 52166.0" + "\n" + 
				"Customer Name: Kelly's Gift Shop|Order Number: 10399|Amount: 52166.0" + "\n",
				test25kQuery.Over25000Query());

	}

}
